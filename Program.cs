﻿using System;
using System.Collections.Generic;
using SFML;
using SFML.System;
using SFML.Window;
using SFML.Graphics;
using System.IO;

namespace Math3D
{

    class Program
    {
        public static List<Mesh> meshesToAdd = new List<Mesh>
        {
            new Cube(new Vector3(0, 0, 0), new Vector3(0.5f, 0.5f, 0.5f), new Color[,]{
                {Color.Red},
                {Color.Blue},
                {Color.Cyan},
                {Color.Green},
                {Color.Magenta},
                {Color.Yellow}
            }),
            new Sphere(new Vector3(0, 0, 0), 0.3f, new Color[,]{
                {Color.Magenta},
                {Color.Blue},
                {Color.Yellow},
                {Color.Green},
                {Color.Cyan},
                {Color.Red}
            }),
            new Cylinder(new Vector3(0, -0.25f, 12f), 0.3f, 0.6f, 20, new Color[]
            {
                Color.Red,
                Color.Blue,
                Color.Yellow,
                Color.Green,
                Color.Cyan,
                Color.Magenta,
                new Color(252, 152, 3),
                new Color(152, 3, 252)
            }),
            new Triangle3D(new Vector3(0,0,0), 
                new Triangle(
                    new Vert(new Vector2(0,0.5f), Color.Red),
                    new Vert(new Vector2(0.5f,0), Color.Blue),
                    new Vert(new Vector2(-0.5f,0), Color.Green)),
                true
            ),
            new Quad3D(Vector3.Zero,
                new Quad(
                    new Vert(new Vector2(0.5f,0.5f), Color.Blue),
                    new Vert(new Vector2(0.5f,-0.5f), Color.Blue),
                    new Vert(new Vector2(-0.5f,-0.5f), Color.Blue),
                    new Vert(new Vector2(-0.5f,0.5f), Color.Blue)
                ), true
            )
        };

        public static List<Mesh> meshes = new List<Mesh>();
        public static List<Text> texts = new List<Text>();

        public static RenderScreen renderScreen;

        public static float rotAngle = 0f;

        public static int meshToAddSelected = 0;

        public static int meshSelected;
        public static bool meshModification = false;
        public static float deltaTime = 1/60f;

        public static void InitMeshesToAdd()
        {
            string path = "Fbx";
            if (Directory.Exists(path))
            {
                foreach (string filename in Directory.GetFiles(path, "*.fbx"))
                {
                    try
                    {
                        TriangleMesh obj = new TriangleMesh(filename, new Vector3(0,0,0), new Color[,] {
                            {Color.Red},
                            {Color.Blue},
                            {Color.Cyan},
                            {Color.Green},
                            {Color.Magenta},
                            {Color.Yellow}
                        });
                        obj.Scale(0.25f);
                        meshesToAdd.Add(obj);
                    } catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        static void Main(string[] args)
        {
            InitMeshesToAdd();
            uint width = 800;
            uint height = 800;
            renderScreen = new RenderScreen(width, height, Color.White);
            renderScreen.useDetphTest = true;

            renderScreen.ProcessingMode = ProcessingMode.GPU;
            RenderWindow window = new RenderWindow(new VideoMode(width,height), "Math3D");
            View view = new View(new FloatRect(0,0,width, height));
            view.Size = new Vector2f(width, -height);
            View textView = new View(new FloatRect(0,0,width, height));
            textView.Size = new Vector2f(width, height);
            window.SetView(view);
            window.Closed += (sender, eventArgs) => ((Window)sender).Close();
            CameraInfo.main = new CameraInfo {
                position = new Vector3(0, 0, 0),
                orientation = new Vector3(0, 0, 0),
                near = 1f,
                far = 1000,
                fieldOfView = 25 * MathF.PI / 180,
                screenSize = new Vector2(width, height),
                projectionType = ProjectionType.perspective,
                renderingMode = RenderingMode.full
            };
            
            window.Resized += (sender, eventArgs) => { 
                
                width = eventArgs.Width;
                height = eventArgs.Height;

                CameraInfo.main.screenSize = new Vector2(width, height);

                view = new View(new FloatRect(0,0,width, height));
                view.Size = new Vector2f(width, -height);
                textView = new View(new FloatRect(0,0,width, height));
                textView.Size = new Vector2f(width, height);
                window.SetView(view);

                renderScreen.UpdateVertices(width, height);
            };

            window.KeyPressed += (sender, eventArgs) =>
            {
                Vector2i mousePos = Mouse.GetPosition(window);
                Vector2 worldPos = window.MapPixelToCoords(mousePos);
                switch (eventArgs.Code)
                {
                    case Keyboard.Key.A: // Add a new object
                        if (Keyboard.IsKeyPressed(Keyboard.Key.LShift) && meshes.Count > 0) {
                            meshes.RemoveAt(meshes.Count - 1);
                            if (meshes.Count == 0)
                            {
                                meshSelected = -1;
                                texts[1].DisplayedString = $"mesh selected: none";
                            }
                            else
                            {
                                meshSelected = meshSelected % meshes.Count;
                                texts[1].DisplayedString = $"mesh selected: {meshes[meshSelected].name}";
                            }
                        } else if (!Keyboard.IsKeyPressed(Keyboard.Key.LShift)){
                            Mesh toAdd = (Mesh)meshesToAdd[meshToAddSelected].Clone();
                            toAdd.Position = CameraInfo.main.position + CameraInfo.main.front * 15;
                            meshes.Add(toAdd);
                        }
                        break;
                    
                    case Keyboard.Key.R: // remove selected object
                        if (meshSelected >= 0)
                        {
                            meshes.RemoveAt(meshSelected);
                            meshSelected = -1;
                            texts[1].DisplayedString = $"mesh selected: none";
                        }
                        break;
                    
                    case Keyboard.Key.P: // Switch between orthographic and perspective projection
                        CameraInfo.main.projectionType = 
                            CameraInfo.main.projectionType == ProjectionType.orthographic ? ProjectionType.perspective : ProjectionType.orthographic;
                        break; 
                    
                    case Keyboard.Key.W: // Switch between Fill and wireframe mode
                        CameraInfo.main.renderingMode = 
                            CameraInfo.main.renderingMode == RenderingMode.full ? RenderingMode.wireframe : RenderingMode.full;
                        break;

                    case Keyboard.Key.Up:
                    case Keyboard.Key.Down:
                    case Keyboard.Key.Right:
                    case Keyboard.Key.Left:
                    case Keyboard.Key.Space:
                    case Keyboard.Key.LControl:
                        if (meshModification) // move or rotate the selected object
                        {
                            if (meshes.Count == 0) break;
                            if (Keyboard.IsKeyPressed(Keyboard.Key.LShift))
                            {
                                // Right now, this is broken
                                Vector3 orientation = new Vector3(0, 0, 0);
                                switch (eventArgs.Code)
                                {
                                    case Keyboard.Key.Right:
                                        orientation.y = 1;
                                        break;

                                    case Keyboard.Key.Left:
                                        orientation.y = -1;
                                        break;

                                    case Keyboard.Key.Up:
                                        orientation.x = -1;
                                        break;

                                    case Keyboard.Key.Down:
                                        orientation.x = 1;
                                        break;

                                    case Keyboard.Key.Space:
                                        orientation.z = 1;
                                        break;

                                    case Keyboard.Key.LControl:
                                        orientation.z = -1;
                                        break;
                                }
                                meshes[meshSelected].Rotate(5 * deltaTime, orientation);
                            }
                            else 
                            {
                                Vector3 offset = new Vector3();
                                switch (eventArgs.Code)
                                {
                                    case Keyboard.Key.Right:
                                        offset = CameraInfo.main.right;
                                        break;

                                    case Keyboard.Key.Left:
                                        offset = -CameraInfo.main.right;
                                        break;

                                    case Keyboard.Key.Up:
                                        offset = 10 * CameraInfo.main.front;
                                        break;

                                    case Keyboard.Key.Down:
                                        offset = 10 * -CameraInfo.main.front;
                                        break;

                                    case Keyboard.Key.Space:
                                        offset = CameraInfo.main.up;
                                        break;

                                    case Keyboard.Key.LControl:
                                        offset = -CameraInfo.main.up;
                                        break;
                                }
                                meshes[meshSelected].Position += 2 * offset * deltaTime;
                            }
                        }
                        else // move or rotate the camera
                        {
                            if (Keyboard.IsKeyPressed(Keyboard.Key.LShift))
                            {
                                Vector3 orientation = new Vector3(0, 0, 0);
                                switch (eventArgs.Code)
                                {
                                    case Keyboard.Key.Right:
                                        orientation.y = 1;
                                        break;

                                    case Keyboard.Key.Left:
                                        orientation.y = -1;
                                        break;

                                    case Keyboard.Key.Up:
                                        orientation.x = -1;
                                        break;

                                    case Keyboard.Key.Down:
                                        orientation.x = 1;
                                        break;
                                }
                                CameraInfo.main.orientation += orientation * deltaTime;
                            }
                            else
                            {
                                Vector3 offset = new Vector3();
                                switch (eventArgs.Code)
                                {
                                    case Keyboard.Key.Right:
                                        offset = CameraInfo.main.right;
                                        break;

                                    case Keyboard.Key.Left:
                                        offset = -CameraInfo.main.right;
                                        break;

                                    case Keyboard.Key.Up:
                                        offset = 10 * CameraInfo.main.front;
                                        break;

                                    case Keyboard.Key.Down:
                                        offset = 10 * -CameraInfo.main.front;
                                        break;

                                    case Keyboard.Key.Space:
                                        offset = CameraInfo.main.up;
                                        break;

                                    case Keyboard.Key.LControl:
                                        offset = -CameraInfo.main.up;
                                        break;
                                }
                                CameraInfo.main.position += 2 * offset * deltaTime;
                            }
                        }
                        break;

                    // Switch between GPU and CPU mode
                    case Keyboard.Key.M:
                        renderScreen.ProcessingMode = renderScreen.ProcessingMode == ProcessingMode.CPU ? ProcessingMode.GPU : ProcessingMode.CPU;
                        break;
                    
                    // Enable/Disable depth test
                    case Keyboard.Key.D:
                        renderScreen.useDetphTest = !renderScreen.useDetphTest;
                        break;
                    
                    // Switch to next coordinate system to show
                    case Keyboard.Key.C:
                        if (renderScreen.coordSystem == CoordSystem.both)
                        {
                            renderScreen.coordSystem = CoordSystem.none;
                        }
                        else
                        {
                            renderScreen.coordSystem++;
                        }
                        break;

                    // Switch between object and camera mode
                    case Keyboard.Key.S:
                        meshModification = !meshModification;
                        texts[0].DisplayedString = meshModification ? "Object mode" : "Camera mode";
                        break;

                    
                    case Keyboard.Key.Tab:
                        if (Keyboard.IsKeyPressed(Keyboard.Key.LShift)) // Change mesh to be added
                        {
                            meshToAddSelected = (meshToAddSelected + 1) % meshesToAdd.Count;
                            texts[2].DisplayedString = $"mesh to add: {meshesToAdd[meshToAddSelected].name}";
                        }
                        else // Change mesh selected with the next one
                        {
                            if (meshes.Count == 0) break;
                            meshSelected = (meshSelected + 1) % meshes.Count;
                            texts[1].DisplayedString = $"mesh selected: {meshes[meshSelected].name}";
                        }
                        break;

                    // Scale the mesh selected
                    case Keyboard.Key.Subtract:
                        if (meshSelected >= 0 && meshSelected < meshes.Count)
                        {
                            meshes[meshSelected].Scale(0.8f);
                        }
                        break;

                    case Keyboard.Key.Add:

                        if (meshSelected >= 0 && meshSelected < meshes.Count)
                        {
                            meshes[meshSelected].Scale(1.2f);
                        }
                        break;
                    
                    // Enable/Disable rotation of all objects
                    case Keyboard.Key.T:
                        rotAngle = rotAngle > 0 ? 0 : 5;
                        break;
                }
            };

            if (meshes.Count != 0)
            {
                meshSelected = 0;
            }

            Font font = new Font("Font/TERBAANG.ttf");
            Text meshModificationText = new Text(meshModification ? "Object mode" : "Camera mode", font, 16);
            meshModificationText.FillColor = Color.Black;
            meshModificationText.Style = Text.Styles.Bold;
            meshModificationText.Position += new Vector2f(10, 0);

            Text meshSelectedText = new Text("mesh selected: none", font, 16);
            meshSelectedText.FillColor = Color.Black;
            meshSelectedText.Style = Text.Styles.Bold;
            meshSelectedText.Position += new Vector2f(10, 20);
            texts.Add(meshModificationText);
            texts.Add(meshSelectedText);

            Text meshToAddText = new Text($"mesh to add: {meshesToAdd[meshToAddSelected].name}", font, 16);
            meshToAddText.FillColor = Color.Black;
            meshToAddText.Style = Text.Styles.Bold;
            meshToAddText.Position += new Vector2f(10, 40);
            texts.Add(meshToAddText);

            deltaTime = 1f/60f;
            Vector3 axis = new Vector3(1,1,1);

            Clock clock = new Clock();
            float elapsedTime = 0;
            int step = 0;
            float fps = 30;
            while (window.IsOpen)
            {
                window.Clear();
                window.DispatchEvents();
                renderScreen.PreUpdate();
                foreach (var mesh in meshes)
                {
                    if (rotAngle > 0) mesh.Rotate(rotAngle * deltaTime, axis);
                    mesh.Draw(renderScreen);
                }
                renderScreen.DrawCoordinateSystem();
                renderScreen.LateUpdate();
                window.Draw(renderScreen.Vertices, PrimitiveType.Points);
                deltaTime = clock.Restart().AsSeconds();
                elapsedTime += deltaTime;
                step++;
                if (elapsedTime >= 1)
                {
                    fps  = step;
                    step = 0;
                    elapsedTime = 0;
                }
                window.SetTitle($"Math3D (FPS : {fps}, mode : {renderScreen.ProcessingMode}), Z-buffer : {(renderScreen.useDetphTest ? "on" : "off")}");
                window.SetView(textView);
                foreach (Text t in texts)
                {
                    window.Draw(t);
                }
                window.SetView(view);
                window.Display();
            }
            renderScreen.Dispose();
        }
    }
}
