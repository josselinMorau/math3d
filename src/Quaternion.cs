using System;

namespace Math3D
{
    ///<summary>
    /// Struct for quaternion.
    ///</summary>
    public struct Quaternion
    {
        public float x;
        public float y;
        public float z;
        public float w;

        public Vector3 Imaginary => new Vector3(x, y, z);

        public float Magnitude => MathF.Sqrt(w*w + x*x + y*y + z*z);

        public Quaternion Conjugate => new Quaternion(-x, -y, -z, w);

        public Quaternion Inverse => Conjugate / Magnitude;

        public Quaternion(float x, float y, float z, float w)
        {
            this.w = w;
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public Quaternion(Vector3 v, float w) : this(v.x, v.y, v.z, w)
        {

        }

        public override string ToString() => $"Quaternion({x}, {y}, {z}, {w})";

        public static Quaternion Identity => new Quaternion(0, 0, 0, 1);

        public static Quaternion Difference(Quaternion a, Quaternion b) => b * a.Inverse;

        public static float Dot(Quaternion a, Quaternion b) => a.w*b.w + a.Imaginary.Dot(b.Imaginary);

        public static Quaternion Log(Quaternion a){
            float mag = a.Magnitude;
            Vector3 v = a.Imaginary;
            return new Quaternion(MathF.Acos((a.w / mag)) * v.Normalized, MathF.Log(mag));
        }

        public static Quaternion Exp(Quaternion a)
        {
            float mag = a.Imaginary.Magnitude;
            return new Quaternion(MathF.Sin(mag) * a.Imaginary / mag, MathF.Cos(mag)) * MathF.Exp(a.w);
        }

        public static Quaternion Pow(Quaternion a, float t)
        {
            float mag = a.Magnitude;
            float theta = MathF.Acos(a.w / mag) * t;
            return new Quaternion(MathF.Sin(theta) * a.Imaginary.Normalized, MathF.Cos(theta)) * MathF.Pow(mag, t);
        }

        public static Quaternion Slerp(Quaternion a, Quaternion b, float t)
        {
            if (t > 1 || t < 0)
            {
                throw new ArgumentException();
            }

            float omega = MathF.Acos(Dot(a, b) / (a.Magnitude * b.Magnitude));
            return (MathF.Sin((1 - t) * omega) * a + MathF.Sin(t * omega) * b) / MathF.Sin(omega);
        }

        public static Quaternion Lerp(Quaternion a, Quaternion b, float t)
        {
            if (t > 1 || t < 0)
            {
                throw new ArgumentException();
            }

            return (1-t)*a + t*b;
        }

        public static Quaternion FromMatrix(Matrix3 m) {

            float w = MathF.Sqrt(m[0, 0] + m[1, 1] + m[2, 2] + 1);
            float x = MathF.Sqrt(m[0, 0] - m[1, 1] - m[2, 2] + 1);
            float y = MathF.Sqrt( - m[0, 0] + m[1, 1] - m[2, 2] + 1);
            float z = MathF.Sqrt( - m[0, 0] - m[1, 1] + m[2, 2] + 1);
            return new Quaternion(w, x, y, z);
        }

        public static Quaternion FromEuler(Vector3 e)
        {
            float w = (float)(Math.Cos(e.y / 2.0) * Math.Cos(e.x / 2.0) * Math.Cos(e.z / 2.0) + Math.Sin(e.y / 2.0) * Math.Sin(e.x / 2.0) * Math.Sin(e.z / 2.0));
            float x = (float)(Math.Cos(e.y / 2.0) * Math.Sin(e.x / 2.0) * Math.Cos(e.z / 2.0) + Math.Sin(e.y / 2.0) * Math.Cos(e.x / 2.0) * Math.Sin(e.z / 2.0));
            float y = (float)(Math.Sin(e.y / 2.0) * Math.Cos(e.x / 2.0) * Math.Cos(e.z / 2.0) - Math.Cos(e.y / 2.0) * Math.Sin(e.x / 2.0) * Math.Sin(e.z / 2.0));
            float z = (float)(Math.Cos(e.y / 2.0) * Math.Cos(e.x / 2.0) * Math.Sin(e.z / 2.0) - Math.Sin(e.y / 2.0) * Math.Sin(e.x / 2.0) * Math.Cos(e.z / 2.0));
            return new Quaternion(x, y, z, w);
        }

        public static Quaternion operator * (float k, Quaternion q) => new Quaternion(q.x*k, q.y*k, q.z*k, q.w*k);

        public static Quaternion operator * (Quaternion q, float k) => k*q;

        public static Quaternion operator *(Quaternion a, Quaternion b)
        {
            Vector3 v1 = a.Imaginary;
            Vector3 v2 = b.Imaginary;

            return new Quaternion(
                a.w * v2 + b.w * v1 + v1.Cross(v2),
                a.w * b.w - v1.Dot(v2)
            );
        }

        public static Quaternion operator -(Quaternion a) => new Quaternion(-a.x, -a.y, -a.z, -a.w);

        public static Quaternion operator -(Quaternion a, Quaternion b) => a + (-b);

        public static Quaternion operator +(Quaternion a, Quaternion b) => new Quaternion(a.x + b.x, a.y + b.y, a.z + b.z, a.w + b.w);
        
        public static Quaternion operator / (Quaternion q, float k) => new Quaternion(q.x/k, q.y/k, q.z/k, q.w/k);
    }
}