using System;
using SFML.Graphics;

namespace Math3D
{
    ///<summary>
    /// Struct for triangle.
    ///</summary>
    public struct Triangle
    {
        public Vert a;

        public Vert b;

        public Vert c;

        public Vector3 normal;

        public Triangle(Color color)
        {
            a = new Vert(new Vector3(), color);
            b = new Vert(new Vector3(), color);
            c = new Vert(new Vector3(), color);
            normal = new Vector3();
        }

        public Triangle(Vert a, Vert b, Vert c) : this(a,b,c,new Vector3())
        {
            
        }

        public Triangle(Vert a, Vert b, Vert c, Vector3 normal)
        {
            this.a = a;
            this.b = b;
            this.c = c;
            this.normal = normal.Normalized;
        }

        public override string ToString() => $"Triangle[{a.position}, {b.position}, {c.position}]";
    }
}