using System;

namespace Math3D
{
    ///<summary>
    /// 4D struct array.
    ///</summary>
    public struct Array4<T> where T : struct
    {
        ///<summary>
        /// First element.
        ///</summary>
        public T first;

        ///<summary>
        /// Second element.
        ///</summary>
        public T second;

        ///<summary>
        /// Third element.
        ///</summary>
        public T third;

        ///<summary>
        /// Third element.
        ///</summary>
        public T fourth;

        ///<summary>
        /// Array4 constructor.
        ///</summary>
        ///<param name="first">First element.</param>
        ///<param name="second">Second element.</param>
        ///<param name="third">Third element.</param>
        ///<param name="fourth">fourth element.</param>
        public Array4(T first, T second, T third, T fourth)
        {
            this.first = first;
            this.second = second;
            this.third = third;
            this.fourth = fourth;
        }

        public T this[int key]
        {
            get
            {
                if (key == 0)
                {
                    return first;
                }
                if (key == 1)
                {
                    return second;
                }
                if (key == 2)
                {
                    return third;
                }
                if (key == 3)
                {
                    return fourth;
                }
                throw new IndexOutOfRangeException();
            }
            set
            {
                if (key == 0)
                {
                    first = value;
                }
                else if (key == 1)
                {
                    second = value;
                }
                else if (key == 2)
                {
                    third = value;
                }
                else if (key == 3)
                {
                    fourth = value;   
                }
                else
                {
                    throw new IndexOutOfRangeException();
                }
            }
        }
    }
}