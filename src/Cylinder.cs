using System;
using SFML.Graphics;

namespace Math3D
{
    public class Cylinder : Mesh
    {
        float radius;

        float height;

        int sections;

        Triangle[] triangles;

        Quad[] quads;

        int[] triangleIndices;

        int[] quadsIndices;

        Color[] colors;

        public Cylinder(Vector3 position, float radius, float height, int section, Color[] colors)
        {
            name = "Cylinder";
            this.position = position;
            this.radius = radius;
            this.height = height;
            this.sections = section >= 2 ? section : 2;
            this.colors = colors;

            float sectionAngle = 2 * MathF.PI / sections;
            verticesPositions = new Vector3[2*sections + 2];
            verticesPositions[0] = position + (height/2) * Vector3.Up;
            verticesPositions[sections+1] = verticesPositions[0] + height * Vector3.Down;

            for (int i=1; i<=sections; i++)
            {
                verticesPositions[i] = verticesPositions[0] + 
                    Vector3.Right * MathF.Cos((i-1)*sectionAngle) * radius + 
                    Vector3.Far * MathF.Sin((i-1)*sectionAngle) * radius;
                verticesPositions[sections + 1 + i] = verticesPositions[i] + height * Vector3.Down;
            }

            triangleIndices = new int[6*sections];
            quadsIndices = new int[4*sections];

            triangles = new Triangle[2*sections];
            quads = new Quad[sections];

            for (int i=0; i<sections; i++)
            {
                bool limit = i+2 > sections;
                Color color = colors[i % colors.Length];
                triangleIndices[3*i] = 0;
                triangleIndices[3*i + 1] = limit ? 1 : i+2;
                triangleIndices[3*i + 2] = i+1;

                triangles[i] = new Triangle(colors[i % colors.Length]);

                triangleIndices[3*(sections + i)] = sections + 1;
                triangleIndices[3*(sections + i) + 1] = sections + i + 2;
                triangleIndices[3*(sections + i) + 2] = limit ? sections + 2 : sections + i + 3;

                triangles[sections + i] = new Triangle(colors[(i + 1) % colors.Length]);

                quadsIndices[4*i] = i+1;
                quadsIndices[4*i + 1] = limit ? 1 : i+2;
                quadsIndices[4*i + 2] = limit ? sections + 2 : sections + i + 3;
                quadsIndices[4*i + 3] = sections + i + 2;

                quads[i] = new Quad(colors[(i + 2) % colors.Length]);
            }

            UpdateFaces();
        }

        public override void UpdateFaces()
        {
            for (int i=0; i<sections; i++)
            {
                triangles[i].a.position = verticesPositions[triangleIndices[3*i]];
                triangles[i].b.position = verticesPositions[triangleIndices[3*i + 1]];
                triangles[i].c.position = verticesPositions[triangleIndices[3*i + 2]];

                triangles[sections + i].a.position = verticesPositions[triangleIndices[3*(sections + i)]];
                triangles[sections + i].b.position = verticesPositions[triangleIndices[3*(sections + i) + 1]];
                triangles[sections + i].c.position = verticesPositions[triangleIndices[3*(sections + i) + 2]];

                quads[i].a.position = verticesPositions[quadsIndices[4*i]];
                quads[i].b.position = verticesPositions[quadsIndices[4*i + 1]];
                quads[i].c.position = verticesPositions[quadsIndices[4*i + 2]];
                quads[i].d.position = verticesPositions[quadsIndices[4*i + 3]];
            }
        }

        public override void Draw(RenderScreen renderScreen)
        {
            foreach (Triangle t in triangles)
            {
                renderScreen.DrawTriangle(t);
            }
            foreach (Quad q in quads)
            {
                renderScreen.DrawQuad(q);
            }
        }

        public override object Clone()
        {
            return new Cylinder(position, radius, height, sections, (Color[])colors.Clone());
        }
    }
}