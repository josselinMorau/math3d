# Math3D

## Prérequis

- Package Nuget SFML pour l'affichage de la fenêtre
- Package Nuget ManagedCuda pour gérer CUDA depuis C#
- CUDA ainsi que son compilateur nvcc

## Notes

Si vous n'avez pas de carte graphique NVIDIA, l'utilisation de CUDA ne sera pas possible et vous ne pourrez utiliser 
que le mode CPU.

## Lancer le programme

Pour lancer le programme il faut d'abord compiler le fichier 'kernel.cu' en .ptx, pour cela exécutez la commande:
`nvcc --ptx kernel.cu` 

Une fois compilé rentrez la commande : `dotnet run`

Vous pouvez aussi directement exécutez le script bash 'run.sh' qui fera ces 2 étapes pour vous.

## Contrôle de la scène

### Déplacements

Il existe deux modes de contrôle:

Mode caméra:

Dans ce mode, les touches fléchées permettent de déplacer la caméra d'avant en arrière (haut/bas) et de gauche à droite.
Pour monter ou descendre il faut appuyer respectivement sur espace et ctrl (gauche).

Pour tourner la caméra de gauche à droite et de haut en bas, il faut utiliser les touches fléchées tout en maintenant shift (gauche).

Mode objet:

Dans ce mode, les déplacements sont identiques à celui de la caméra mais modifieront à la place la position et la rotation
de l'objet sélectionné dans "Mesh selected". Pour changer l'objet sélectionné il faut appuyer sur Tab.

### Raccourcis

Voici une liste de raccourcis claviers pour contrôler la scène : 

- A : Ajoute l'objet indiqué dans "Mesh to add" devant la caméra
- Shift+A : Supprime le dernier objet ajouté
- R : Supprime l'objet sélectionné dans "Mesh selected"
- Tab : Change l'objet sélectionné dans "Mesh selected"
- Shift+Tab : Change l'objet à ajouter dans "Mesh to add"
- S : Active/Désactive entre le mode caméra et objet
- M : Altèrne entre le mode CPU et le mode GPU
- P : Altèrne entre le mode projection perspective et orthographique
- W : Altèrne entre le mode forme pleine et fil de fer
- D : Active/Désactive le test de profondeur (z-buffer)
- C : Altèrne entre : Ne pas montrer de repère, montrer le repère caméra, montrer le repère monde, montrer les deux repères
- T : Active/Désactive la rotation constante de tous les objets de la scène
- \+ : Aggrandit l'objet sélectionné dans "Mesh selected"
- \-  : Rétrécit l'objet sélectionné dans "Mesh selected"
