extern "C"  {

    typedef struct Color
    {
        unsigned char R;
        unsigned char G;
        unsigned char B;
        unsigned char A;
    } Color;

    typedef struct Vector2f
    {
        float X;
        float Y;
    } Vector2f;

    typedef struct Vertex
    {
        Vector2f Position;
        Color Color;
        Vector2f TexCoords;
    } Vertex;


    __device__ float3 GetPseudoBarycentricCoordinates(float2 p, float2 a, float2 b, float2 c)
    {
        float3 bary;
        bary.x = (p.x - b.x) * (c.y - b.y) - (p.y - b.y) * (c.x - b.x);
        bary.y = (p.x - c.x) * (a.y - c.y) - (p.y - c.y) * (a.x - c.x);
        bary.z = (p.x - a.x) * (b.y - a.y) - (p.y - a.y) * (b.x - a.x);
        return bary;
    }

    __device__ float3 ComputeWorldPos(float3 bary, float3 a, float3 b, float3 c)
    {
        float3 wpos;
        wpos.x = 1.0f / ((1.0f / a.x) * bary.x + (1.0f / b.x) * bary.y + (1.0f / c.x) * bary.z);
        wpos.y = 1.0f / ((1.0f / a.y) * bary.x + (1.0f / b.y) * bary.y + (1.0f / c.y) * bary.z);
        wpos.z = 1.0f / ((1.0f / a.z) * bary.x + (1.0f / b.z) * bary.y + (1.0f / c.z) * bary.z);
        return wpos;
    }

    __device__ Color GetColorTriangulation(float3 coef, Color a, Color b, Color c)
    {
        Color color;
        color.R = (unsigned char)(coef.x * a.R + coef.y * b.R + coef.z * c.R);
        color.G = (unsigned char)(coef.x * a.G + coef.y * b.G + coef.z * c.G);
        color.B = (unsigned char)(coef.x * a.B + coef.y * b.B + coef.z * c.B);
        return color;
    }
    
    __global__ void ComputeTriangle(
        Vertex* vertices, float* depths, int width, int2 posMin, 
        int N, int L, int H, float area, int depthTest,
        Color ca, Color cb, Color cc,
        float3 wa, float3 wb, float3 wc,
        float2 a, float2 b, float2 c
    )
    {
        int k = threadIdx.x + blockIdx.x * blockDim.x;
        if (k >= L*H) return;
        k = (k % L) + posMin.x + (k/L + posMin.y) * width;
        if (k < 0 || k >= N) return;

        Vector2f v = vertices[k].Position;
        float2 pos;
        pos.x = v.X;
        pos.y = v.Y;
        float3 bary = GetPseudoBarycentricCoordinates(pos, a, b, c);
        if (bary.x < 0 || bary.y < 0 || bary.z < 0) return;

        bary.x /= area;
        bary.y /= area;
        bary.z /= area;
        float3 worldPos = ComputeWorldPos(bary, wa, wb, wc);
        float z = worldPos.z;
        if ((depthTest == 0 || z < depths[k]) && z > 0)
        {
            if (depthTest == 1) depths[k] = z;
            vertices[k].Color = GetColorTriangulation(bary, ca, cb, cc);
        }
    }

    __global__ void ComputeSegment(
        Vertex* vertices, Color color, int width, int height, int xMin, int xMax,
        int yMin, int yMax, int yOfXmin, int yOfXmax,
        int deltaX, int deltaY, int useX, int straightLine
    )
    {
        int k = threadIdx.x + blockIdx.x * blockDim.x;
        if ((useX && k > deltaX) || (!useX && k > yMax - yMin)) return;
        int x, y;
        if (useX)
        {
            x = xMin + k;
            if (straightLine)
            {
                y = yMin;
            }
            else
            {
                y = deltaY * (x - xMin) / deltaX + yOfXmin;
            }
        }
        else
        {
            y = yMin + k;
            if (straightLine)
            {
                x = xMin;
            }
            else
            {
                x = (y - yOfXmin) * deltaX / deltaY + xMin;
            }
        }
        if (x < 0 || x >= width || y < 0 || y >= height) return;
        vertices[y * width + x].Color = color;
    }

    __global__ void ClearBuffers(Vertex* vertices, Color defaultColor, float* depths, float far, int N)
    {
        int i = threadIdx.x + blockIdx.x * blockDim.x;
        if (i >= N) return;
        vertices[i].Color = defaultColor;
        depths[i] = far;
    }
}