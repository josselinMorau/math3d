using System;

namespace Math3D
{
    ///<summary>
    /// 2D struct array.
    ///</summary>
    public struct Array2<T> where T : struct
    {
        ///<summary>
        /// First element.
        ///</summary>
        public T first;

        ///<summary>
        /// Second element.
        ///</summary>
        public T second;

        ///<summary>
        /// Array2 constructor.
        ///</summary>
        ///<param name="first">First element.</param>
        ///<param name="second">Second element.</param>
        public Array2(T first, T second)
        {
            this.first = first;
            this.second = second;
        }

        public T this[int key]
        {
            get
            {
                if (key == 0)
                {
                    return first;
                }
                if (key == 1)
                {
                    return second;
                }
                throw new IndexOutOfRangeException();
            }
            set
            {
                if (key == 0)
                {
                    first = value;
                }
                else if (key == 1)
                {
                    second = value;
                }
                else
                {
                    throw new IndexOutOfRangeException();
                }
            }
        }
    }
}