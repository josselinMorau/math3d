using System;
using SFML.Graphics;

namespace Math3D
{
    ///<summary>
    /// Base class for 3D objects in the scene.
    ///</summary>
    public abstract class Mesh : ICloneable
    {
        protected Vector3[] verticesPositions;

        protected Vector3 position;

        public string name;

        public virtual Vector3 Position
        {
            get => position;
            set
            {
                Vector3 offset = value - position;
                position = value;
                for (int i=0; i<verticesPositions.Length; i++)
                {
                    verticesPositions[i] += offset;
                }
                UpdateFaces();
            }
        }

        public abstract object Clone();

        public virtual void UpdateFaces(){}

        public abstract void Draw(RenderScreen renderScreen);

        public virtual void Rotate(float angle, Vector3 axis)
        {
            for (int i=0; i<verticesPositions.Length; i++)
            {
                verticesPositions[i] -= position;
                verticesPositions[i] *= Matrix3.Rotation(angle, axis);
                verticesPositions[i] += position;
            }
            UpdateFaces();
        }

        public virtual void Scale(float factor)
        {
            if (factor <= 0)
            {
                return;
            }
            for (int i = 0; i < verticesPositions.Length; i++)
            {
                verticesPositions[i] -= position;
                verticesPositions[i] *= factor;
                verticesPositions[i] += position;
            }
            UpdateFaces();
        }
    }
}