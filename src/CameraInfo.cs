using System;

namespace Math3D
{
    ///<summary>
    /// Infos on camera.
    ///</summary>
    public struct CameraInfo
    {
        public Vector3 position;

        Vector3 orientation_;

        public Vector3 orientation
        {
            get => orientation_;
            set
            {
                orientation_ = value;
                Matrix3 rot = Matrix3.Rotation(orientation_);
                front_ = Vector3.Far * rot;
                right_ = Vector3.Right * rot;
                up_ = Vector3.Up * rot;
            }
        }

        float near_;

        public float near
        {
            get => near_;
            set
            {
                near_ = value;
                float s = 2 * near_ * MathF.Tan(fieldOfView_ / 2);
                size = new Vector2(s,s);
            }
        }

        public Vector2 size {get; private set;}

        public float far;

        float fieldOfView_;

        public float fieldOfView
        {
            get => fieldOfView_;
            set
            {
                fieldOfView_ = value;
                float s = 2 * near_ * MathF.Tan(fieldOfView_ / 2);
                size = new Vector2(s,s);
            }
        }

        public Vector2 screenSize;

        public ProjectionType projectionType;

        public RenderingMode renderingMode;

        Vector3 front_;

        public Vector3 front => front_;

        Vector3 up_;

        public Vector3 up => up_;

        Vector3 right_;

        public Vector3 right => right_;

        public Vector3 center => position + near * front;

        public bool IsInScreen(Vector2 pos) => pos.x >= 0 && pos.x < screenSize.x && pos.y >= 0 && pos.y < screenSize.y;
        public bool IsInScreen(int x, int y) => x >= 0 && x < screenSize.x && y >= 0 && y < screenSize.y;

        public static CameraInfo main;
    }

}