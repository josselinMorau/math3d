using System;
using SFML.System;
using ManagedCuda.VectorTypes;

namespace Math3D
{
    ///<summary>
    /// Struct for 4D vectors.
    ///</summary>
    public struct Vector4
    {
        ///<summary>
        /// The x-axis component of the Vector.
        ///</summary>
        public float x;

        ///<summary>
        /// The y-axis component of the Vector.
        ///</summary>
        public float y;

        ///<summary>
        /// The z-axis component of the Vector.
        ///</summary>
        public float z;

        ///<summary>
        /// The w-axis component of the Vector.
        ///</summary>
        public float w;

        ///<summary>
        /// Constructor for 4D vector.
        ///</summary>
        ///<param name="x">The x-axis component of the Vector.</param>
        ///<param name="y">The y-axis component of the Vector.</param>
        ///<param name="z">The z-axis component of the Vector.</param>
        ///<param name="w">The w-axis component of the Vector.</param>
        public Vector4(float x, float y, float z, float w)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.w = w;
        }

        ///<summary>
        /// Constructor for 4D vector.
        ///</summary>
        ///<param name="xy">A Vector2 representing the x and y components.</param>
        ///<param name="z">The z-axis component of the Vector.</param>
        ///<param name="w">The w-axis component of the Vector.</param>
        public Vector4(Vector2 xy, float z, float w) : this(xy.x, xy.y, z, w)
        {
        }

        ///<summary>
        /// Constructor for 4D vector.
        ///</summary>
        ///<param name="xyz">A Vector3 representing the x, y and z components.</param>
        ///<param name="w">The w-axis component of the Vector.</param>
        public Vector4(Vector3 xyz, float w) : this(xyz.x, xyz.y, xyz.z, w)
        {
        }

        ///<summary>
        /// Returns a string representation of the vector.
        ///</summary>
        public override String ToString() => $"Vector[{x}, {y}, {z}, {w}]";

        public override int GetHashCode() => base.GetHashCode();

        public override bool Equals(object obj)
        {
            if (!(obj is Vector4))
                return false;

            return (Vector4)obj == this;
        }

        public float this[int key]
        {
            get
            {
                if (key == 0)
                {
                    return x;
                }
                else if (key == 1)
                {
                    return y;
                }
                else if (key == 2)
                {
                    return z;
                }
                else if (key == 3)
                {
                    return w;
                }
                else
                {
                    throw new IndexOutOfRangeException();
                }
            }

            set
            {
                if (key == 0)
                {
                    x = value;
                }
                else if (key == 1)
                {
                    y = value;
                }
                else if (key == 2)
                {
                    z = value;
                }
                else if (key == 3)
                {
                    w = value;
                }
                else
                {
                    throw new IndexOutOfRangeException();
                }
            }
        }

        ///<summary>
        /// The Vector's magnitude.
        ///</summary>
        public float Magnitude => MathF.Sqrt(x * x + y * y + z * z + w * w);

        ///<summary>
        /// The normalization of the Vector.
        ///</summary>
        public Vector4 Normalized => this / this.Magnitude;

        ///<summary>
        /// Normalizes the Vector.
        ///</summary>
        public void Normalize()
        {
            float magnitude = this.Magnitude;
            this.x /= magnitude;
            this.y /= magnitude;
            this.z /= magnitude;
            this.w /= magnitude;
        }

        ///<summary>
        /// Returns the distance between the current vector and an other one.
        ///</summary>
        ///<param name="v">The other vector.</param>
        public float GetDistance(Vector4 v) => DistanceBetween(this, v);

        ///<summary>
        ///Returns the dot product of the current vector with an other one.
        ///</summary>
        ///<param name="v">The other vector.</param>
        public float Dot(Vector4 v) => DotBetween(this, v);

        ///<summary>
        /// Returns the angle between the current vector and an other one.
        ///</summary>
        ///<param name="v">The other vector.</param>
        public float GetAngle(Vector4 v) => AngleBetween(this, v);

        public float4 ToFloat4() => new float4(x, y, z, w);

        ///<summary>
        ///Returns the distance between two vectors.
        ///</summary>
        ///<param name="v1">The first vector.</param>
        ///<param name="v2">The second vector.</param>
        public static float DistanceBetween(Vector4 v1, Vector4 v2) => (v1-v2).Magnitude;

        ///<summary>
        ///Returns the dot product of two vectors.
        ///</summary>
        ///<param name="v1">The first operand in the product.</param>
        ///<param name="v2">The second operand in the product.</param>
        public static float DotBetween(Vector4 v1, Vector4 v2) => v1.x * v2.x + v1.y * v2.y + v1.z * v2.z + v1.w * v2.w;

        ///<summary>
        /// Returns the angle between 2 vectors (in radians).
        ///</summary>
        ///<param name="v1">The first vector.</param>
        ///<param name="v2">The second vector.</param>
        public static float AngleBetween(Vector4 v1, Vector4 v2) => MathF.Acos(DotBetween(v1,v2) / (v1.Magnitude * v2.Magnitude));

        ///<summary>
        /// Returns the angle between 2 vectors.
        ///</summary>
        ///<param name="v1">The first vector.</param>
        ///<param name="v2">The second vector.</param>
        ///<param name="inDegrees">Whether the result should be in degrees or in radians.</param>
        public static float AngleBetween(Vector4 v1, Vector4 v2, bool inDegrees)
        {
            float angle = AngleBetween(v1, v2);
            return inDegrees ? angle * 180 / MathF.PI : angle;
        }

        ///<summary>
        /// Shortcut for Vector(0,0,0,0);
        ///</summary>
        public static Vector4 Zero => new Vector4(0,0,0,0);

        public static Vector4 operator -(Vector4 v) => new Vector4(-v.x, -v.y, -v.z, -v.w);

        public static Vector4 operator +(Vector4 v, Vector4 w) => new Vector4(v.x + w.x, v.y + w.y, v.z + w.z, v.w + w.w);

        public static Vector4 operator -(Vector4 v, Vector4 w) => v + (-w);

        public static Vector4 operator *(float k, Vector4 v) => new Vector4(k*v.x, k*v.y, k*v.z, k*v.w);

        public static Vector4 operator *(Vector4 v, float k) => k*v;

        public static Vector4 operator *(Vector4 v1, Vector4 v2) => new Vector4(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z, v1.w * v2.w);

        public static Vector4 operator /(Vector4 v1, Vector4 v2) => new Vector4(v1.x / v2.x, v1.y / v2.y, v1.z / v2.z, v1.w / v2.w);

        public static Vector4 operator *(Vector4 v, Matrix4 m) => new Vector4(v.Dot(m.GetColumn(0)), v.Dot(m.GetColumn(1)), v.Dot(m.GetColumn(2)), v.Dot(m.GetColumn(3)));

        public static Vector4 operator /(Vector4 v, float k) => new Vector4(v.x/k, v.y/k, v.z/k, v.w/k);

        public static Vector4 operator /(float x, Vector4 v) => new Vector4(x / v.x, x / v.y, x / v.z, x / v.w);

        public static implicit operator Vector4(Vector2 v) => new Vector4(v.x, v.y, 0, 0);

        public static implicit operator Vector4(Vector3 v) => new Vector4(v.x, v.y, v.z, 0);

        public static implicit operator Vector2f(Vector4 v) => new Vector2f(v.x, v.y);

        public static implicit operator Vector3f(Vector4 v) => new Vector3f(v.x, v.y, v.z);

        public static bool operator ==(Vector4 v1, Vector4 v2) => v1.x == v2.x && v1.y == v2.y && v1.z == v2.z;
        
        public static bool operator !=(Vector4 v1, Vector4 v2) => !(v1 == v2);
    }
}