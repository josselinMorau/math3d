using System;

namespace Math3D
{
    ///<summary>
    /// Struct for 3*3 matrix.
    ///</summary>
    public struct Matrix3
    {
        private const int SIZE = 3;

        ///<summary>
        /// The rows of the matrix.
        ///</summary>
        public Array3<Vector3> rows;

        ///<summary>
        /// The transpose of the matrix.
        ///</summary>
        public Matrix3 Transpose {
            get {
                Matrix3 res = this;
                for(int i = 0; i < SIZE; ++i) {
                    for (int j = 0; j < SIZE; ++j) {
                        res[i, j] = this[j, i];
                    }
                }
                return res;
            }
        }

        ///<summary>
        /// The determinant of the matrix.
        ///</summary>
        public float Determinant
        {
            get 
            {
                return this[0,0] * this[1,1] * this[2,2]
                    + this[1,0] * this[2,1] * this[0,2]
                    + this[2,0] * this[0,1] * this[1,2]
                    - this[0,2] * this[1,1] * this[2,0]
                    - this[1,2] * this[2,1] * this[0,0]
                    - this[2,2] * this[0,1] * this[1,0];
            }
        }

        ///<summary>
        /// The inverse of the matrix.
        ///</summary>
        public Matrix3 Inverse
        {
            get
            {
                float det = Determinant;
                if (det == 0)
                {
                    throw new ArithmeticException("Non-inversible matrix.");
                }

                Matrix3 res = -this;
                res[0,0] = this[1,1];
                res[1,1] = this[0,0];
                return res/det;
            }
        }

        public Matrix3(float[,] values)
        {
            if (values.GetLength(0) != SIZE || values.GetLength(1) != SIZE)
            {
                throw new IndexOutOfRangeException();
            }

            rows = new Array3<Vector3> (
                new Vector3(values[0,0], values[0,1], values[0,2]),
                new Vector3(values[1,0], values[1,1], values[1,2]),
                new Vector3(values[2,0], values[2,1], values[2,2])
            );
        }

        public Matrix3(Vector3 row1, Vector3 row2, Vector3 row3)
        {
            rows = new Array3<Vector3>(row1, row2, row3);
        }

        public float this[int i, int j]
        {
            get => rows[i][j];
            set { 
                Vector3 v = rows[i];
                v[j] = value;
                rows[i] = v;
            }
        }

        ///<summary>
        /// Returns a string representation of the matrix.
        ///</summary>
        public override String ToString() 
            => $"[[{this[0, 0]}, {this[0, 1]}, {this[0, 2]}]\n"
             + $" [{this[1, 0]}, {this[1, 1]}, {this[1, 2]}]\n"
             + $" [{this[2, 0]}, {this[2, 1]}, {this[2, 2]}]]\n";

        public override int GetHashCode() => base.GetHashCode();

        ///<summary>
        /// Checks equality on the given object.
        ///</summary>
        public override bool Equals(object obj)
        {
            if (!(obj is Matrix3))
                return false;

            return (Matrix3)obj == this;
        }

        ///<summary>
        /// Returns the row with the given index.
        ///</summary>
        ///<param name="i">The index of the row.</param>
        public Vector3 GetRow(int i) => rows[i];

        ///<summary>
        /// Returns the column with the given index.
        ///</summary>
        ///<param name="i">The index of the column.</param>
        public Vector3 GetColumn(int i)
        {
            if (i >= SIZE)
            {
                throw new IndexOutOfRangeException();
            }

            return new Vector3(this[0,i], this[1,i], this[2,i]);
        }

        ///<summary>
        /// Returns a rotation matrix.
        ///</summary>
        ///<param name="angle">The angle of rotation (in radians).</param>
        ///<param name="axis">The axis on which the rotation is performed.</param>
        public static Matrix3 Rotation(float angle, Vector3 axis)
        {
            float cost = MathF.Cos(angle);
            float sint = MathF.Sin(angle);
            axis.Normalize();

            if (axis == Vector3.Right)
            {
                return new Matrix3(
                    new Vector3(1, 0, 0),
                    new Vector3(0, cost, sint),
                    new Vector3(0, -sint, cost)
                );
            }

            if (axis == Vector3.Up)
            {
                return new Matrix3(
                    new Vector3(cost, 0, -sint),
                    new Vector3(0, 1, 0),
                    new Vector3(sint, 0, cost)
                );
            }
            
            if (axis == Vector3.Far)
            {
                return new Matrix3(
                    new Vector3(cost, sint, 0),
                    new Vector3(-sint, cost, 0),
                    new Vector3(0, 0, 1)
                );
            }

            return new Matrix3(
                new Vector3(axis.x*axis.x*(1-cost)+cost, axis.x*axis.y*(1-cost)+axis.z*sint, axis.x*axis.z*(1-cost)-axis.y*sint),
                new Vector3(axis.x*axis.y*(1-cost)-axis.z*sint, axis.y*axis.y*(1-cost)+cost, axis.y*axis.z*(1-cost)+axis.x*sint),
                new Vector3(axis.x*axis.z*(1-cost)+axis.y*sint, axis.z*axis.y*(1-cost)-axis.x*sint, axis.z*axis.z*(1-cost)+cost)
            );
        }

        ///<summary>
        /// Returns a rotation matrix.
        ///</summary>
        ///<param name="euler">Euler angles.</param>
        public static Matrix3 Rotation(Vector3 euler)
        {
            return Rotation(euler.x, Vector3.Right) * Rotation(euler.y, Vector3.Up) * Rotation(euler.z, Vector3.Far);
        }

        ///<summary>
        /// The identity matrix.
        ///</summary>
        public static Matrix3 Identity => new Matrix3(new float[3,3] {{1,0,0}, {0,1,0}, {0,0,1}});

        public static Matrix3 operator +(Matrix3 m1, Matrix3 m2) 
            => new Matrix3(
                m1.GetRow(0) + m2.GetRow(0), 
                m1.GetRow(1) + m2.GetRow(1),
                m1.GetRow(2) + m2.GetRow(2));

        public static Matrix3 operator -(Matrix3 m) => new Matrix3(-m.GetRow(0), -m.GetRow(1), -m.GetRow(2));

        public static Matrix3 operator -(Matrix3 m1, Matrix3 m2) => m1 + (-m2);

        public static Matrix3 operator *(Matrix3 m, float k) => new Matrix3(k*m.GetRow(0), k*m.GetRow(1), k*m.GetRow(2));

        public static Matrix3 operator *(float k, Matrix3 m) => m * k;

        public static Matrix3 operator *(Matrix3 m1, Matrix3 m2)
        {
            Matrix3 res = new Matrix3();
            for (int i=0; i<SIZE; i++)
            {
                for (int j=0; j<SIZE; j++)
                {
                    res[i,j] = m1.GetRow(i).Dot(m2.GetColumn(j));
                }
            }
            return res;
        }

        public static Matrix3 operator /(Matrix3 m, float k) => new Matrix3(m.GetRow(0) / k, m.GetRow(1) / k, m.GetRow(2));

        public static bool operator ==(Matrix3 m1, Matrix3 m2)
        {
            for (int i=0; i<SIZE; i++)
            {
                for (int j=0; j<SIZE; j++)
                {
                    if (m1[i,j] != m2[i,j])
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        
        public static bool operator !=(Matrix3 m1, Matrix3 m2) => !(m1 == m2);

        public static implicit operator Matrix3(Matrix2 m) => new Matrix3(m.GetRow(0), m.GetRow(1), new Vector3(0,0,1));

        public static implicit operator Matrix3(Matrix4 m) => new Matrix3(m.GetRow(0), m.GetRow(1), m.GetRow(2));
    }
}