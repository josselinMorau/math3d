using System;
using System.Linq;
using SFML.Graphics;
using UkooLabs.FbxSharpie;
using System.Collections.Generic;


namespace Math3D
{
    ///<summary>
    /// Mesh represented by an array of triangles.
    ///</summary>
    public class TriangleMesh : Mesh
    {
        Triangle[] triangles;

        TriangleMesh(string name, Vector3 position, Vector3[] vertices, Triangle[] triangles)
        {
            this.name = name;
            this.position = position;
            this.verticesPositions = vertices;
            this.triangles = triangles;
        }

        public TriangleMesh(String fbxPath, Vector3 position, Color[,] colors)
        {
            name = fbxPath.Split(new char[]{'/', '\\'}).Last().Split('.')[0];
            this.position = position;

            int m = colors.GetLength(0), n = colors.GetLength(1);

            int nbVertex = 0;

            bool isBinary = FbxIO.IsBinaryFbx(fbxPath);
            FbxDocument documentNode = FbxIO.Read(fbxPath);

            List<Vector3> verticesPositionsList = new List<Vector3>();
            List<Color> cols = new List<Color>();
            long[] geometryIds = documentNode.GetGeometryIds();

            foreach (long geometryId in geometryIds)
            {
                int[] vertexIndices = documentNode.GetVertexIndices(geometryId);
                System.Numerics.Vector3[] positions = documentNode.GetPositions(geometryId, vertexIndices);

                nbVertex += positions.Length;

                for (var i = 0; i < positions.Length; i++)
                {
                    verticesPositionsList.Add(position + new Vector3(positions[i]));
                    cols.Add(colors[i % m, i % n]);
                }
            }
            verticesPositions = verticesPositionsList.ToArray();

            triangles = new Triangle[verticesPositions.Length / 3];
            for (int k = 0; k < (verticesPositions.Length / 3); k++)
            {
                triangles[k] = new Triangle(
                    new Vert(verticesPositions[3 * k], cols[3 * k]),
                    new Vert(verticesPositions[3 * k + 1], cols[3 * k + 1]),
                    new Vert(verticesPositions[3 * k + 2], cols[3 * k + 2])
                );
            }
        }
        public override void UpdateFaces() {
            for (int k = 0; k < (verticesPositions.Length / 3); k++)
            {
                triangles[k].a.position = verticesPositions[3 * k];
                triangles[k].b.position = verticesPositions[3 * k + 1];
                triangles[k].c.position = verticesPositions[3 * k + 2];
            }
        }

        public override void Draw(RenderScreen renderScreen)
        {
            foreach (Triangle t in triangles)
            {
                renderScreen.DrawTriangle(t);
            }
        }

        Triangle[] Triangulate(Vector3[] vertices)
        {
            Triangle[] triangles = new Triangle[vertices.Length];
            
            return triangles;
        }

        public override object Clone()
        {
            return new TriangleMesh(name, position, (Vector3[])verticesPositions.Clone(), (Triangle[])triangles.Clone());
        }
    }
}