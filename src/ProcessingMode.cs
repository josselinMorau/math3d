
///<summary>
/// The mode of processing used (either CPU or GPU).
///</summary>
public enum ProcessingMode
{
    CPU,
    GPU
}