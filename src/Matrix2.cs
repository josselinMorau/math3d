using System;

namespace Math3D
{
    ///<summary>
    /// Struct for 2*2 matrix.
    ///</summary>
    public struct Matrix2
    {
        private const int SIZE = 2;

        ///<summary>
        /// The rows of the matrix.
        ///</summary>
        public Array2<Vector2> rows;

        ///<summary>
        /// The transpose of the matrix.
        ///</summary>
        public Matrix2 Transpose {
            get {
                Matrix2 res = this;
                for(int i = 0; i < SIZE; ++i) {
                    for (int j = 0; j < SIZE; ++j) {
                        res[i, j] = this[j, i];
                    }
                }
                return res;
            }
        }

        ///<summary>
        /// The determinant of the matrix.
        ///</summary>
        public float Determinant => this[0,0] * this[1,1] - this[1,0] * this[0,1];

        ///<summary>
        /// The inverse of the matrix.
        ///</summary>
        public Matrix2 Inverse
        {
            get
            {
                float det = Determinant;
                if (det == 0)
                {
                    throw new ArithmeticException("Non-inversible matrix.");
                }

                Matrix2 res = -this;
                res[0,0] = this[1,1];
                res[1,1] = this[0,0];
                return res/det;
            }
        }

        public Matrix2(float[,] values)
        {
            if (values.GetLength(0) != SIZE || values.GetLength(1) != SIZE)
            {
                throw new IndexOutOfRangeException();
            }

            rows = new Array2<Vector2> (
                new Vector2(values[0,0], values[0,1]),
                new Vector2(values[1,0], values[1,1])
            );
        }

        public Matrix2(Vector2 row1, Vector2 row2)
        {
            rows = new  Array2<Vector2>(row1, row2);
        }

        public float this[int i, int j]
        {
            get => rows[i][j];
            set { 
                Vector2 v = rows[i];
                v[j] = value;
                rows[i] = v;
            }
        }

        ///<summary>
        /// Returns a string representation of the matrix.
        ///</summary>
        public override String ToString() => $"[[{this[0, 0]}, {this[0, 1]}]\n [{this[1, 0]}, {this[1, 1]}]]";

        public override int GetHashCode() => base.GetHashCode();

        ///<summary>
        /// Checks equality on the given object.
        ///</summary>
        public override bool Equals(object obj)
        {
            if (!(obj is Matrix2))
                return false;

            return (Matrix2)obj == this;
        }

        ///<summary>
        /// Returns the row with the given index.
        ///</summary>
        ///<param name="i">The index of the row.</param>
        public Vector2 GetRow(int i) => rows[i];

        ///<summary>
        /// Returns the column with the given index.
        ///</summary>
        ///<param name="i">The index of the column.</param>
        public Vector2 GetColumn(int i)
        {
            if (i >= SIZE)
            {
                throw new IndexOutOfRangeException();
            }

            return new Vector2(this[0,i], this[1,i]);
        }

        ///<summary>
        /// Returns a rotation matrix.
        ///</summary>
        ///<param name="angle">The rotation angle (in radians).</param>
        public static Matrix2 Rotate(float angle)
        {
            float cost = MathF.Cos(angle);
            float sint = MathF.Sin(angle);
            return new Matrix2(new Vector2(cost, -sint), new Vector2(sint, cost));
        }

        ///<summary>
        /// Returns a rotation matrix.
        ///</summary>
        ///<param name="angle">The rotation angle.</param>
        ///<param name="inDegrees">Whether the angle is in degrees or in radians.</param>
        public static Matrix2 Rotate(float angle, bool inDegrees)
        {
            float angleRad = inDegrees ? angle * MathF.PI / 180 : angle;
            return Rotate(angleRad);
        }

        ///<summary>
        /// Returns a scaling matrix.
        ///</summary>
        ///<param name="kx">The scaling factor along the x-axis.</param>
        ///<param name="ky">The scaling factor along the y-axis.</param>
        public static Matrix2 Scale(float kx, float ky) => new Matrix2(new Vector2(kx, 0), new Vector2(0, ky));

        ///<summary>
        /// Returns a scaling matrix.
        ///</summary>
        ///<param name="k">The scaling factor along the x and y-axis.</param>
        public static Matrix2 Scale(float k) => Scale(k,k);

        ///<summary>
        /// The identity matrix.
        ///</summary>
        public static Matrix2 Identity => new Matrix2(new Vector2(1,0), new Vector2(0,1));

        public static Matrix2 operator +(Matrix2 m1, Matrix2 m2) => new Matrix2(m1.GetRow(0) + m2.GetRow(0), m1.GetRow(1) + m2.GetRow(1));

        public static Matrix2 operator -(Matrix2 m) => new Matrix2(-m.GetRow(0), -m.GetRow(1));

        public static Matrix2 operator -(Matrix2 m1, Matrix2 m2) => m1 + (-m2);

        public static Matrix2 operator *(Matrix2 m, float k) => new Matrix2(k*m.GetRow(0), k*m.GetRow(1));

        public static Matrix2 operator *(float k, Matrix2 m) => m * k;

        public static Matrix2 operator *(Matrix2 m1, Matrix2 m2)
        {
            Matrix2 res = new Matrix2();
            for (int i=0; i<SIZE; i++)
            {
                for (int j=0; j<SIZE; j++)
                {
                    res[i,j] = m1.GetRow(i).Dot(m2.GetColumn(j));
                }
            }
            return res;
        }

        public static Matrix2 operator /(Matrix2 m, float k) => new Matrix2(m.GetRow(0) / k, m.GetRow(1) / k);

        public static bool operator ==(Matrix2 m1, Matrix2 m2)
        {
            for (int i=0; i<SIZE; i++)
            {
                for (int j=0; j<SIZE; j++)
                {
                    if (m1[i,j] != m2[i,j])
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        
        public static bool operator !=(Matrix2 m1, Matrix2 m2) => !(m1 == m2);

        public static implicit operator Matrix2(Matrix3 m) => new Matrix2(m.GetRow(0), m.GetRow(1));

        public static implicit operator Matrix2(Matrix4 m) => new Matrix2(m.GetRow(0), m.GetRow(1));
    }
}