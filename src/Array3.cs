using System;

namespace Math3D
{
    ///<summary>
    /// 3D struct array.
    ///</summary>
    public struct Array3<T> where T : struct
    {
        ///<summary>
        /// First element.
        ///</summary>
        public T first;

        ///<summary>
        /// Second element.
        ///</summary>
        public T second;

        ///<summary>
        /// Third element.
        ///</summary>
        public T third;

        ///<summary>
        /// Array3 constructor.
        ///</summary>
        ///<param name="first">First element.</param>
        ///<param name="second">Second element.</param>
        ///<param name="third">Thrid element.</param>
        public Array3(T first, T second, T third)
        {
            this.first = first;
            this.second = second;
            this.third = third;
        }

        public T this[int key]
        {
            get
            {
                if (key == 0)
                {
                    return first;
                }
                if (key == 1)
                {
                    return second;
                }
                if (key == 2)
                {
                    return third;
                }
                throw new IndexOutOfRangeException();
            }
            set
            {
                if (key == 0)
                {
                    first = value;
                }
                else if (key == 1)
                {
                    second = value;
                }
                else if (key == 2)
                {
                    third = value;
                }
                else
                {
                    throw new IndexOutOfRangeException();
                }
            }
        }
    }
}