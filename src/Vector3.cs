using System;
using SFML.System;
using ManagedCuda.VectorTypes;

namespace Math3D
{
    ///<summary>
    /// Struct for 3D vectors.
    ///</summary>
    public struct Vector3
    {
        ///<summary>
        /// The x-axis component of the Vector.
        ///</summary>
        public float x;

        ///<summary>
        /// The y-axis component of the Vector.
        ///</summary>
        public float y;

        ///<summary>
        /// The z-axis component of the Vector.
        ///</summary>
        public float z;

        ///<summary>
        /// Constructor for 3D vector.
        ///</summary>
        ///<param name="x">The x-axis component of the Vector.</param>
        ///<param name="y">The y-axis component of the Vector.</param>
        ///<param name="z">The z-axis component of the Vector.</param>
        public Vector3(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        ///<summary>
        /// Constructor for 3D vector.
        ///</summary>
        ///<param name="xy">A Vector2 representing the x and y components.</param>
        ///<param name="z">The z-axis component of the Vector.</param>
        public Vector3(Vector2 xy, float z) : this(xy.x, xy.y, z)
        {
        }

        ///<summary>
        /// Constructor for 3D vector.
        ///</summary>
        ///<param name="v">A SFML vector.</param>
        public Vector3(Vector3f v) : this(v.X, v.Y, v.Z)
        {
        }
        
        ///<summary>
        /// Constructor for 3D vector.
        ///</summary>
        ///<param name="v">A System.Numerics vector.</param>
        public Vector3(System.Numerics.Vector3 v) : this(v.X, v.Y, v.Z)
        {
        }

        ///<summary>
        /// Returns a string representation of the vector.
        ///</summary>
        public override String ToString() => $"Vector[{x}, {y}, {z}]";

        public override int GetHashCode() => base.GetHashCode();

        public override bool Equals(object obj)
        {
            if (!(obj is Vector3))
                return false;

            return (Vector3)obj == this;
        }

        public float this[int key]
        {
            get
            {
                if (key == 0)
                {
                    return x;
                }
                else if (key == 1)
                {
                    return y;
                }
                else if (key == 2)
                {
                    return z;
                }
                else
                {
                    throw new IndexOutOfRangeException();
                }
            }

            set
            {
                if (key == 0)
                {
                    x = value;
                }
                else if (key == 1)
                {
                    y = value;
                }
                else if (key == 2)
                {
                    z = value;
                }
                else
                {
                    throw new IndexOutOfRangeException();
                }
            }
        }

        ///<summary>
        /// The Vector's magnitude.
        ///</summary>
        public float Magnitude => MathF.Sqrt(x * x + y * y + z * z);

        ///<summary>
        /// The normalization of the Vector.
        ///</summary>
        public Vector3 Normalized => this / this.Magnitude;

        ///<summary>
        /// Normalizes the Vector.
        ///</summary>
        public void Normalize()
        {
            float magnitude = this.Magnitude;
            this.x /= magnitude;
            this.y /= magnitude;
            this.z /= magnitude;
        }

        ///<summary>
        /// Returns the distance between the current vector and an other one.
        ///</summary>
        ///<param name="v">The other vector.</param>
        public float GetDistance(Vector3 v) => DistanceBetween(this, v);

        ///<summary>
        ///Returns the dot product of the current vector with an other one.
        ///</summary>
        ///<param name="v">The other vector.</param>
        public float Dot(Vector3 v) => DotBetween(this, v);

        ///<summary>
        ///Returns the cross product of the current vector with an other one.
        ///</summary>
        ///<param name="v">The other vector.</param>
        public Vector3 Cross(Vector3 v) => CrossBetween(this, v);

        ///<summary>
        /// Returns the angle between the current vector and an other one.
        ///</summary>
        ///<param name="v">The other vector.</param>
        public float GetAngle(Vector3 v) => AngleBetween(this, v);

        ///<summary>
        /// Returns the equivalent vector in SFML standard.
        ///</summary>
        public Vector3f ToVector3f() => new Vector3f(x, y, z);

        public void Rotate(Vector3 euler)
        {
            Rotate(Quaternion.FromEuler(euler));
        }

        public void Rotate(Quaternion q)
        {
            Vector3 u = q.Imaginary;
            float w = q.w;
            float dot = u.Dot(this);
            Vector3 v = 2 * dot * u + (w*w - dot) * this + 2 * w * u.Cross(this);
            this.x = v.x;
            this.y = v.y;
            this.z = v.z;
        }

        public float3 ToFloat3() => new float3(x, y, z);

        ///<summary>
        /// Shortcut for Vector(0,0,0);
        ///</summary>
        public static Vector3 Zero => new Vector3(0,0,0);

        ///<summary>
        /// Shortcut for Vector(1,0,0);
        ///</summary>
        public static Vector3 Right => new Vector3(1,0,0);

        ///<summary>
        /// Shortcut for Vector(-1,0,0);
        ///</summary>
        public static Vector3 Left => new Vector3(-1,0,0);

        ///<summary>
        /// Shortcut for Vector(0,1,0);
        ///</summary>
        public static Vector3 Up => new Vector3(0,1,0);

        ///<summary>
        /// Shortcut for Vector(0,-1,0);
        ///</summary>
        public static Vector3 Down => new Vector3(0,-1,0);

        ///<summary>
        /// Shortcut for Vector(0,0,1);
        ///</summary>
        public static Vector3 Far => new Vector3(0,0,1);

        ///<summary>
        /// Shortcut for Vector(0,0,-1);
        ///</summary>
        public static Vector3 Near => new Vector3(0,0,-1);

        ///<summary>
        ///Returns the distance between two vectors.
        ///</summary>
        ///<param name="v1">The first vector.</param>
        ///<param name="v2">The second vector.</param>
        public static float DistanceBetween(Vector3 v1, Vector3 v2) => (v1-v2).Magnitude;

        ///<summary>
        ///Returns the dot product of two vectors.
        ///</summary>
        ///<param name="v1">The first operand in the product.</param>
        ///<param name="v2">The second operand in the product.</param>
        public static float DotBetween(Vector3 v1, Vector3 v2) => v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;

        ///<summary>
        ///Returns the cross product of two vectors.
        ///</summary>
        ///<param name="v1">The first operand in the product.</param>
        ///<param name="v2">The second operand in the product.</param>
        public static Vector3 CrossBetween(Vector3 v, Vector3 w) 
            => new Vector3(
                v.y * w.z - w.y * v.z,
                v.x * w.z - w.x * v.z,
                v.y * w.x - w.y * v.x
                );

        ///<summary>
        /// Returns the angle between 2 vectors (in radians).
        ///</summary>
        ///<param name="v1">The first vector.</param>
        ///<param name="v2">The second vector.</param>
        public static float AngleBetween(Vector3 v1, Vector3 v2) => MathF.Acos(DotBetween(v1,v2) / (v1.Magnitude * v2.Magnitude));

        ///<summary>
        /// Returns the angle between 2 vectors.
        ///</summary>
        ///<param name="v1">The first vector.</param>
        ///<param name="v2">The second vector.</param>
        ///<param name="inDegrees">Whether the result should be in degrees or in radians.</param>
        public static float AngleBetween(Vector3 v1, Vector3 v2, bool inDegrees)
        {
            float angle = AngleBetween(v1, v2);
            return inDegrees ? angle * 180 / MathF.PI : angle;
        }

        public static Vector3 operator -(Vector3 v) => new Vector3(-v.x, -v.y, -v.z);

        public static Vector3 operator +(Vector3 v, Vector3 w) => new Vector3(v.x + w.x, v.y + w.y, v.z + w.z);

        public static Vector3 operator -(Vector3 v, Vector3 w) => v + (-w);

        public static Vector3 operator *(float k, Vector3 v) => new Vector3(k*v.x, k*v.y, k*v.z);

        public static Vector3 operator *(Vector3 v, float k) => k*v;

        public static Vector3 operator *(Vector3 v1, Vector3 v2) => new Vector3(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z);

        public static Vector3 operator /(Vector3 v1, Vector3 v2) => new Vector3(v1.x / v2.x, v1.y / v2.y, v1.z / v2.z);

        public static Vector3 operator *(Vector3 v, Matrix3 m) => new Vector3(v.Dot(m.GetColumn(0)), v.Dot(m.GetColumn(1)), v.Dot(m.GetColumn(2)));

        public static Vector3 operator /(Vector3 v, float k) => new Vector3(v.x/k, v.y/k, v.z/k);

        public static Vector3 operator /(float x, Vector3 v) => new Vector3(x / v.x, x / v.y, x / v.z);

        public static implicit operator Vector3(Vector2 v) => new Vector3(v.x, v.y, 0);

        public static implicit operator Vector3(Vector4 v) => new Vector3(v.x, v.y, v.z);

        public static implicit operator Vector3(Vector3f v) => new Vector3(v);
        
        public static implicit operator Vector2f(Vector3 v) => new Vector2f(v.x, v.y);
        
        public static implicit operator Vector3f(Vector3 v) => new Vector3f(v.x, v.y, v.z);

        public static bool operator ==(Vector3 v1, Vector3 v2) => v1.x == v2.x && v1.y == v2.y && v1.z == v2.z;
        
        public static bool operator !=(Vector3 v1, Vector3 v2) => !(v1 == v2);
    }
}