using System;
using SFML.Graphics;

namespace Math3D
{
    ///<summary>
    /// Struct for quad.
    ///</summary>
    public struct Quad
    {
        public Vert a;

        public Vert b;

        public Vert c;

        public Vert d;

        public Vector3 normal;

        public Quad(Color color)
        {
            a = new Vert(new Vector3(), color);
            b = new Vert(new Vector3(), color);
            c = new Vert(new Vector3(), color);
            d = new Vert(new Vector3(), color);
            normal = new Vector3();
        }

        public Quad(Vert a, Vert b, Vert c, Vert d) : this(a, b, c, d, new Vector3())
        {
        }

        public Quad(Vert a, Vert b, Vert c, Vert d, Vector3 normal)
        {
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
            this.normal = normal.Normalized;
        }
    }
}