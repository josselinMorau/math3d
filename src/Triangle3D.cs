
namespace Math3D
{
    public class Triangle3D : Mesh
    {

        Triangle[] triangles;

        bool doubleFace;

        public Triangle3D(Vector3 position, Triangle triangle, bool doubleFace = false)
        {
            name = "Triangle";
            this.position = position;
            Vector3 center = (triangle.a.position + triangle.b.position + triangle.c.position) / 3;
            triangle.a.position = position + (triangle.a.position - center);
            triangle.b.position = position + (triangle.b.position - center);
            triangle.c.position = position + (triangle.c.position - center);
            this.doubleFace = doubleFace;
            int n = doubleFace ? 2 : 1;
            triangles = new Triangle[n];
            verticesPositions = new Vector3[3];
            verticesPositions[0] = triangle.a.position;
            verticesPositions[1] = triangle.b.position;
            verticesPositions[2] = triangle.c.position;
            triangles[0] = triangle;

            if (doubleFace)
            {
                triangles[1] = new Triangle(
                    triangle.a, triangle.c, triangle.b
                );
            }
        }

        public override void UpdateFaces()
        {
            triangles[0].a.position = verticesPositions[0];
            triangles[0].b.position = verticesPositions[1];
            triangles[0].c.position = verticesPositions[2];

            if (doubleFace)
            {
                triangles[1].a.position = verticesPositions[0];
                triangles[1].b.position = verticesPositions[2];
                triangles[1].c.position = verticesPositions[1];
            }
        }

        public override void Draw(RenderScreen renderScreen)
        {
            renderScreen.DrawTriangle(triangles[0]);
            if (doubleFace)
            {
                renderScreen.DrawTriangle(triangles[1]);
            }
        }

        public override object Clone()
        {
            return new Triangle3D(position, triangles[0], doubleFace);
        }
    }
}