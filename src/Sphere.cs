using System;
using System.Collections.Generic;
using SFML.Graphics;

namespace Math3D
{
    class Sphere : Mesh
    {
        float radius;

        Color[,] colors;

        int longitude;

        int latitude;

        Triangle[] triangles;
        List<int> indices = new List<int>();

        public Sphere(Vector3 center, float radius, Color[,] colors, int sectorCount = 10, int stackCount = 10)
        {
            name = "Sphere";
            this.position = center;
            this.radius = radius;
            this.colors = colors;
            this.longitude = sectorCount;
            this.latitude = stackCount;

            int m = colors.GetLength(0), n = colors.GetLength(1);
            float x, y, z, xy;

            float sectorStep = 2 * MathF.PI / longitude;
            float stackStep = MathF.PI / latitude;
            float sectorAngle, stackAngle;

            verticesPositions = new Vector3[(latitude + 1) * (longitude + 1)];
            List<Color> cols = new List<Color>();

            for (int i = 0; i <= latitude; ++i)
            {
                stackAngle = MathF.PI / 2 - i * stackStep;
                xy = radius * MathF.Cos(stackAngle);
                z = position.z + radius * MathF.Sin(stackAngle);

                for (int j = 0; j <= longitude; ++j)
                {
                    sectorAngle = j * sectorStep;

                    x = position.x + xy * MathF.Cos(sectorAngle);
                    y = position.y + xy * MathF.Sin(sectorAngle);
                    verticesPositions[i * (longitude + 1) + j] = new Vector3(x, y, z);
                    cols.Add(colors[i % m, j % n]);
                }
            }

            int k1, k2;
            for (int i = 0; i < latitude; ++i)
            {
                k1 = i * (longitude + 1);
                k2 = k1 + longitude + 1;

                for (int j = 0; j < longitude; ++j, ++k1, ++k2)
                {

                    if (i != 0)
                    {
                        indices.Add(k1);
                        indices.Add(k2);
                        indices.Add(k1 + 1);
                    }

                    if (i != (latitude - 1))
                    {
                        indices.Add(k1 + 1);
                        indices.Add(k2);
                        indices.Add(k2 + 1);
                    }
                }
            }

            triangles = new Triangle[indices.Count / 3];
            for (int k = 0; k < (indices.Count / 3); k++)
            {
                int l = longitude + 1;
                triangles[k] = new Triangle(
                    new Vert(verticesPositions[indices[3 * k]], cols[indices[3 * k]]),
                    new Vert(verticesPositions[indices[3 * k + 1]], cols[indices[3 * k + 1]]),
                    new Vert(verticesPositions[indices[3 * k + 2]], cols[indices[3 * k + 2]])
                );
            }
            cols.Clear();
        }

        public override void UpdateFaces()
        {
            for (int k = 0; k < (indices.Count / 3); k++)
            {
                triangles[k].a.position = verticesPositions[indices[3 * k]];
                triangles[k].b.position = verticesPositions[indices[3 * k + 1]];
                triangles[k].c.position = verticesPositions[indices[3 * k + 2]];
            }
        }

        public override void Draw(RenderScreen renderScreen)
        {
            foreach (Triangle triangle in triangles)
            {
                renderScreen.DrawTriangle(triangle);
            }
        }

        public override object Clone()
        {
            return new Sphere(position, radius, (Color[,])colors.Clone(), longitude, latitude);
        }
    }
}