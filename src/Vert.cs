using System;
using SFML.Graphics;

namespace Math3D
{
    ///<summary>
    /// Struct for vertex.
    ///</summary>
    public struct Vert
    {
        public Vector3 position;

        public Color color;

        public Vert(Vector3 pos, Color color)
        {
            this.position = pos;
            this.color = color;
        }

        public static implicit operator Vertex(Vert v) => new Vertex(v.position, v.color);

        public static implicit operator Vert(Vertex v) => new Vert(new Vector2(v.Position), v.Color);
    }
}