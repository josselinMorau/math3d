using System;
using SFML.System;
using ManagedCuda.VectorTypes;

namespace Math3D
{
    ///<summary>
    /// Struct for 2D vectors.
    ///</summary>
    public struct Vector2
    {
        ///<summary>
        /// The x-axis component of the Vector.
        ///</summary>
        public float x;

        ///<summary>
        /// The y-axis component of the Vector.
        ///</summary>
        public float y;

        ///<summary>
        /// Constructor for 2D vector.
        ///</summary>
        ///<param name="x">The x-axis component of the Vector.</param>
        ///<param name="y">The y-axis component of the Vector.</param>
        public Vector2(float x, float y)
        {
            this.x = x;
            this.y = y;
        }

        ///<summary>
        /// Constructor for 2D vector.
        ///</summary>
        ///<param name="v">A SFML vector.</param>
        public Vector2(Vector2f v) : this(v.X, v.Y)
        {
        }

        ///<summary>
        /// Returns a string representation of the vector.
        ///</summary>
        public override String ToString() => $"Vector[{x}, {y}]";

        public override int GetHashCode() => base.GetHashCode();

        ///<summary>
        /// Checks equality on the given object.
        ///</summary>
        public override bool Equals(object obj)
        {
            if (!(obj is Vector2))
                return false;

            return (Vector2)obj == this;
        }

        public float this[int key]
        {
            get
            {
                if (key == 0)
                {
                    return x;
                }
                if (key == 1)
                {
                    return y;
                }
                throw new IndexOutOfRangeException();
            }

            set
            {
                if (key == 0)
                {
                    x = value;
                }
                else if (key == 1)
                {
                    y = value;
                }
                else
                {
                    throw new IndexOutOfRangeException();
                }
            }
        }

        ///<summary>
        /// The Vector's magnitude.
        ///</summary>
        public float Magnitude => MathF.Sqrt(x * x + y * y);

        ///<summary>
        /// The normalization of the Vector.
        ///</summary>
        public Vector2 Normalized => this / this.Magnitude;

        ///<summary>
        /// Normalizes the Vector.
        ///</summary>
        public void Normalize()
        {
            float magnitude = this.Magnitude;
            this.x /= magnitude;
            this.y /= magnitude;
        }

        ///<summary>
        /// Returns the distance between the current vector and an other one.
        ///</summary>
        ///<param name="v">The other vector.</param>
        public float GetDistance(Vector2 v) => DistanceBetween(this, v);

        ///<summary>
        ///Returns the dot product of the current vector with an other one.
        ///</summary>
        ///<param name="v">The other vector.</param>
        public float Dot(Vector2 v) => DotBetween(this, v);

        ///<summary>
        /// Returns the angle between the current vector and an other one.
        ///</summary>
        ///<param name="v">The other vector.</param>
        public float GetAngle(Vector2 v) => AngleBetween(this, v);

        ///<summary>
        /// Returns the equivalent vector in SFML standard.
        ///</summary>
        public Vector2f ToVector2f() => new Vector2f(x, y);

        public float2 ToFloat2() => new float2(x, y);

        ///<summary>
        /// Shortcut for Vector(0,0);
        ///</summary>
        public static Vector2 Zero => new Vector2(0,0);

        ///<summary>
        /// Shortcut for Vector(1,0);
        ///</summary>
        public static Vector2 Right => new Vector2(1,0);

        ///<summary>
        /// Shortcut for Vector(-1,0);
        ///</summary>
        public static Vector2 Left => new Vector2(-1,0);

        ///<summary>
        /// Shortcut for Vector(0,1);
        ///</summary>
        public static Vector2 Up => new Vector2(0,1);

        ///<summary>
        /// Shortcut for Vector(0,-1);
        ///</summary>
        public static Vector2 Down => new Vector2(0,-1);

        ///<summary>
        ///Returns the distance between two vectors.
        ///</summary>
        ///<param name="v1">The first vector.</param>
        ///<param name="v2">The second vector.</param>
        public static float DistanceBetween(Vector2 v1, Vector2 v2) => (v1-v2).Magnitude;

        ///<summary>
        ///Returns the dot product of two vectors.
        ///</summary>
        ///<param name="v1">The first operand in the product.</param>
        ///<param name="v2">The second operand in the product.</param>
        public static float DotBetween(Vector2 v1, Vector2 v2) => v1.x * v2.x + v1.y * v2.y;

        ///<summary>
        /// Returns the angle between 2 vectors (in radians).
        ///</summary>
        ///<param name="v1">The first vector.</param>
        ///<param name="v2">The second vector.</param>
        public static float AngleBetween(Vector2 v1, Vector2 v2) => MathF.Acos(DotBetween(v1,v2) / (v1.Magnitude * v2.Magnitude));

        ///<summary>
        /// Returns the angle between 2 vectors.
        ///</summary>
        ///<param name="v1">The first vector.</param>
        ///<param name="v2">The second vector.</param>
        ///<param name="inDegrees">Whether the result should be in degrees or in radians.</param>
        public static float AngleBetween(Vector2 v1, Vector2 v2, bool inDegrees)
        {
            float angle = AngleBetween(v1, v2);
            return inDegrees ? angle * 180 / MathF.PI : angle;
        }

        public static Vector2 operator -(Vector2 v) => new Vector2(-v.x, -v.y);

        public static Vector2 operator +(Vector2 v, Vector2 w) => new Vector2(v.x + w.x, v.y + w.y);

        public static Vector2 operator -(Vector2 v, Vector2 w) => v + (-w);

        public static Vector2 operator *(float k, Vector2 v) => new Vector2(k*v.x, k*v.y);

        public static Vector2 operator *(Vector2 v, float k) => k*v;

        public static Vector2 operator *(Vector2 v1, Vector2 v2) => new Vector2(v1.x * v2.x, v1.y * v2.y);

        public static Vector2 operator /(Vector2 v1, Vector2 v2) => new Vector2(v1.x / v2.x, v1.y / v2.y);

        public static Vector2 operator /(float x, Vector2 v) => new Vector2(x / v.x, x / v.y);

        public static Vector2 operator *(Vector2 v, Matrix2 m) => new Vector2(v.Dot(m.GetColumn(0)), v.Dot(m.GetColumn(1)));

        public static Vector2 operator /(Vector2 v, float k) => new Vector2(v.x/k, v.y/k);
        
        public static implicit operator Vector2(Vector3 v) => new Vector2(v.x, v.y);

        public static implicit operator Vector2(Vector4 v) => new Vector2(v.x, v.y);

        public static implicit operator Vector2(Vector2f v) => new Vector2(v);

        public static implicit operator Vector2f(Vector2 v) => new Vector2f(v.x, v.y);

        public static implicit operator Vector3f(Vector2 v) => new Vector3f(v.x, v.y, 0);

        public static bool operator ==(Vector2 v1, Vector2 v2) => v1.x == v2.x && v1.y == v2.y;
        
        public static bool operator !=(Vector2 v1, Vector2 v2) => !(v1 == v2);
    }
}