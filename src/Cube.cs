using System;
using SFML.Graphics;

namespace Math3D
{
    public class Cube : Mesh
    {
        Vector3 size;

        int[] cubeFaces;

        Quad[] quads;

        Color[,] colors;

        public Cube(Vector3 position, Vector3 size, Color[,] colors)
        {
            this.colors = colors;
            name = "Cube";
            this.size = size;
            verticesPositions = new Vector3[8];
            quads = new Quad[6];

            int m = colors.GetLength(0), n = colors.GetLength(1);

            verticesPositions[0] = position + size * new Vector3(-0.5f, 0.5f, -0.5f);
            verticesPositions[1] = position + size * new Vector3(0.5f, 0.5f, -0.5f);
            verticesPositions[2] = position + size * new Vector3(0.5f, -0.5f, -0.5f);
            verticesPositions[3] = position + size * new Vector3(-0.5f, -0.5f, -0.5f);
            verticesPositions[4] = position + size * new Vector3(-0.5f, 0.5f, 0.5f);
            verticesPositions[5] = position + size * new Vector3(0.5f, 0.5f, 0.5f);
            verticesPositions[6] = position + size * new Vector3(0.5f, -0.5f, 0.5f);
            verticesPositions[7] = position + size * new Vector3(-0.5f, -0.5f, 0.5f);

            for (int i=0; i<6; i++)
            {
                quads[i] = new Quad();
                quads[i].a.color = colors[i % m, 0];
                quads[i].b.color = colors[i % m, 1 % n];
                quads[i].c.color = colors[i % m, 2 % n];
                quads[i].d.color = colors[i % m, 3 % n];
            }

            cubeFaces = new int[] {0, 1, 2, 3, 1, 5, 6, 2, 5, 4, 7, 6, 4, 0, 3, 7, 4, 5, 1, 0, 3, 2, 6, 7};

            this.position = position;
            UpdateFaces();
        }

        public override void UpdateFaces()
        {
            for (int i=0; i<6; i++)
            {
                quads[i].a.position = verticesPositions[cubeFaces[i*4]];
                quads[i].b.position = verticesPositions[cubeFaces[i*4 + 1]];
                quads[i].c.position = verticesPositions[cubeFaces[i*4 + 2]];
                quads[i].d.position = verticesPositions[cubeFaces[i*4 + 3]];
            }
        }

        public override void Draw(RenderScreen renderScreen)
        {
            foreach (var q in quads)
            {
                renderScreen.DrawQuad(q);
            }
        }

        public override object Clone()
        {
            return new Cube(position, size, (Color[,])colors.Clone());
        }
    }
}