
namespace Math3D
{
    public class Quad3D : Mesh
    {

        Quad[] quads;

        bool doubleFace;

        public Quad3D(Vector3 position, Quad quad, bool doubleFace = false)
        {
            name = "Quad";
            this.position = position;
            Vector3 center = (quad.a.position + quad.b.position + quad.c.position + quad.d.position) / 4;
            quad.a.position = position + (quad.a.position - center);
            quad.b.position = position + (quad.b.position - center);
            quad.c.position = position + (quad.c.position - center);
            quad.d.position = position + (quad.d.position - center);
            this.doubleFace = doubleFace;
            int n = doubleFace ? 2 : 1;
            quads = new Quad[n];
            verticesPositions = new Vector3[4];
            verticesPositions[0] = quad.a.position;
            verticesPositions[1] = quad.b.position;
            verticesPositions[2] = quad.c.position;
            verticesPositions[3] = quad.d.position;
            quads[0] = quad;

            if (doubleFace)
            {
                quads[1] = new Quad(
                    quad.a, quad.d, quad.c, quad.b
                );
            }
        }

        public override void UpdateFaces()
        {
            quads[0].a.position = verticesPositions[0];
            quads[0].b.position = verticesPositions[1];
            quads[0].c.position = verticesPositions[2];
            quads[0].d.position = verticesPositions[3];

            if (doubleFace)
            {
                quads[1].a.position = verticesPositions[0];
                quads[1].b.position = verticesPositions[3];
                quads[1].c.position = verticesPositions[2];
                quads[1].d.position = verticesPositions[1];
            }
        }

        public override void Draw(RenderScreen renderScreen)
        {
            renderScreen.DrawQuad(quads[0]);
            if (doubleFace)
            {
                renderScreen.DrawQuad(quads[1]);
            }
        }

        public override object Clone()
        {
            return new Quad3D(position, quads[0], doubleFace);
        }
    }
}