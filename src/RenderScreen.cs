using System;
using SFML.System;
using SFML.Graphics;
using ManagedCuda;
using ManagedCuda.VectorTypes;

namespace Math3D
{

    public class RenderScreen : IDisposable
    {   
        Vertex[] vertices;

        public Vertex[] Vertices => vertices;

        float[] depthMap;

        public float[] DepthMap => depthMap;

        public Color backgroundColor;

        uint height;

        uint width;

        public bool useDetphTest = true;

        bool supportCuda;

        ProcessingMode processingMode;

        CudaContext cudaContext = null;

        CudaKernel cudaKernelTriangle = null;

        CudaKernel cudaKernelClear = null;

        CudaKernel cudaKernelWireframe = null;

        CudaDeviceVariable<Vertex> d_vertices;

        CudaDeviceVariable<float> d_depthMap;

        dim3 maxBlockDim;

        int maxThreadsPerBlock;

        public ProcessingMode ProcessingMode
        {
            get => processingMode;
            set
            {
                processingMode = !supportCuda ? ProcessingMode.CPU : value;
            }
        }

        public CoordSystem coordSystem;
        
        static CudaContext LoadCudaContext()
        {
            try
            {
                return new CudaContext();
            }
            catch
            {
                return null;
            }
        }

        public RenderScreen(uint width, uint height) : this(width, height, Color.Black) {}

        public RenderScreen(uint width, uint height, Color backgroundColor)
        {
            this.coordSystem = CoordSystem.none;
            this.backgroundColor = backgroundColor;

            cudaContext = LoadCudaContext();
            if (cudaContext != null)
            {
                CudaDeviceProperties props = cudaContext.GetDeviceInfo();
                maxThreadsPerBlock = props.MaxThreadsPerBlock;
                maxBlockDim = props.MaxBlockDim;
                
                supportCuda = maxBlockDim.Size > 0 && maxThreadsPerBlock > 0;
                ProcessingMode = ProcessingMode.GPU;
            }
            else
            {
                supportCuda = false;
                ProcessingMode = ProcessingMode.CPU;
            }

            UpdateVertices(width, height);
        }

        public void Dispose()
        {
            if (supportCuda)
            {
                d_vertices.Dispose();
                d_depthMap.Dispose();
                cudaContext.Dispose();
            }
        }

        ///<summary>
        /// Must be called before each frame.
        ///</summary>
        public void PreUpdate()
        {
            if (ProcessingMode == ProcessingMode.GPU)
            {
                try
                {
                    if (cudaKernelClear == null)
                    {
                        cudaKernelClear = cudaContext.LoadKernel("kernel.ptx", "ClearBuffers");
                    }
                    else
                    {
                        int nbThreads = vertices.Length;
                        cudaKernelClear.BlockDimensions = Math.Min(nbThreads, maxThreadsPerBlock);
                        cudaKernelClear.GridDimensions = (cudaKernelClear.BlockDimensions + nbThreads - 1) / cudaKernelClear.BlockDimensions;
                        cudaKernelClear.Run(
                            d_vertices.DevicePointer, backgroundColor,
                            d_depthMap.DevicePointer, CameraInfo.main.far, vertices.Length);
                    }
                    //d_vertices.CopyToDevice(vertices);
                    //d_depthMap.CopyToDevice(depthMap);
                } catch (Exception e)
                {
                    Console.WriteLine(
                        $"An error occured while calling the GPU rendering function : {e.Message}" + 
                        "\nRenderScreen switched back to CPU mode.");
                    ProcessingMode = ProcessingMode.CPU;
                    Clear();
                }
            }
            else
            {
                Clear();
            }
        }

        ///<summary>
        /// Must be called after each frame.
        ///</summary>
        public void LateUpdate()
        {
            if (ProcessingMode == ProcessingMode.GPU)
            {
                try
                {
                    d_vertices.CopyToHost(vertices);
                } catch (Exception e)
                {
                    Console.WriteLine(
                        $"An error occured while calling the GPU rendering function : {e.Message}" + 
                        "\nRenderScreen switched back to CPU mode.");
                    ProcessingMode = ProcessingMode.CPU;
                }
            }
        }

        public void UpdateVertices(uint width, uint height)
        {
            this.width = width;
            this.height = height;
            uint n = width * height;
            vertices = new Vertex[n];
            for (int i = 0; i< height; i++)
            {
                for (int j=0; j< width; j++)
                {
                    vertices[i*width + j] = new Vertex(new Vector2f(j, i));
                }
            }
            depthMap = new float[n];
            if (ProcessingMode == ProcessingMode.GPU)
            {
                try
                {
                    d_vertices = new CudaDeviceVariable<Vertex>(n);
                    d_depthMap = new CudaDeviceVariable<float>(n);
                    d_vertices.CopyToDevice(vertices);
                    d_depthMap.CopyToDevice(depthMap);
                } catch (Exception e)
                {
                    Console.WriteLine(
                        $"An error occured while calling the GPU rendering function : {e.Message}" + 
                        "\nRenderScreen switched back to CPU mode.");
                    ProcessingMode = ProcessingMode.CPU;
                }
            }
        }

        void Clear()
        {
            for (int i=0; i < height; i++)
            {
                for (int j=0; j < width; j++)
                {
                    vertices[i*width + j] = new Vertex(new Vector2f(j, i), backgroundColor);
                    depthMap[i*width + j] = CameraInfo.main.far;
                }
            }
        }

        static float EdgeFunction(Vector3 a, Vector3 b, Vector3 c) => (c.x - a.x) * (b.y - a.y) - (c.y - a.y) * (b.x - a.x);

        static Color GetColorTriangulation(Vector3 coefficients, Color a, Color b, Color c)
        {
            Color color = new Color(0,0,0);
            color.R = (byte)(coefficients.x * a.R + coefficients.y * b.R + coefficients.z * c.R);
            color.G = (byte)(coefficients.x * a.G + coefficients.y * b.G + coefficients.z * c.G);
            color.B = (byte)(coefficients.x * a.B + coefficients.y * b.B + coefficients.z * c.B);
            return color;
        }

        static Color GetColorMean(Color a, Color b, Color c)
        {
            Color color = new Color(0,0,0);
            color.R = (byte)((a.R + b.R + c.R) / 3);
            color.G = (byte)((a.G + b.G + c.G) / 3);
            color.B = (byte)((a.B + b.B + c.B) / 3);
            return color;
        }

        static Vector3 GetPseudoBarycentricCoordinates(Vector3 p, Vector3 a, Vector3 b, Vector3 c)
        {
            return new Vector3(
                EdgeFunction(b, c, p),
                EdgeFunction(c, a, p),
                EdgeFunction(a, b, p)
            );
        }

        void DrawTriangleCPU(
            int xMin, int xMax, int yMin, int yMax, float area,
            Vector2 screenA, Vector2 screenB, Vector2 screenC,
            Vert a, Vert b, Vert c
        )
        {
            int x0 = Math.Max(0, xMin);
            int xn = Math.Min((int)width-1, xMax);
            int y0 = Math.Max(0,yMin);
            int yn = Math.Min((int)height-1, yMax);
            for (int i=x0; i<=xn; i++)
            {
                for (int j=y0; j<=yn; j++)
                {
                    Vector2 pos = new Vector2(i,j);
                    if (!CameraInfo.main.IsInScreen(pos))
                    {
                        continue;
                    }

                    Vector3 bary = GetPseudoBarycentricCoordinates(pos, screenA, screenB, screenC);
                    //check if point is in triangle
                    if (bary.x >= 0 && bary.y >= 0 && bary.z >= 0)
                    {
                        int k = (int)(j * width + i);
                        bary /= area;
                        Vector3 worldPos = 1 / ((1 / a.position) * bary.x + (1 / b.position) * bary.y + (1 / c.position) * bary.z);
                        float z = worldPos.z;
                        if (useDetphTest && z < depthMap[k] && z > 0)
                        {
                            Color color = GetColorTriangulation(bary, a.color, b.color, c.color);//GetColorMean(a.color, b.color, c.color);
                            depthMap[k] = z;
                            vertices[k].Color = color;
                        }
                        else if (z >= 0)
                        {
                            Color color = GetColorTriangulation(bary, a.color, b.color, c.color);
                            vertices[k].Color = color;
                        }
                    }
                }
            }
        }

        void DrawSegment(Vector2 a, Vector2 b, Color col)
        {
            int xMin, xMax, yOfXMin, yOfXMax, yMin, yMax, x, y;
            xMin = (int)MathF.Floor(MathF.Min(a.x, b.x));
            xMax = (int)MathF.Floor(MathF.Max(a.x, b.x));
            yMin = (int)MathF.Floor(MathF.Min(a.y, b.y));
            yMax = (int)MathF.Floor(MathF.Max(a.y, b.y));

            if (xMin == (int)MathF.Floor(a.x))
            {
                yOfXMin = (int)MathF.Floor(a.y);
                yOfXMax = (int)MathF.Floor(b.y);
            } else
            {
                yOfXMin = (int)MathF.Floor(b.y);
                yOfXMax = (int)MathF.Floor(a.y);
            }

            bool useX = MathF.Abs(xMax - xMin) >= MathF.Abs(yMax - yMin) || yMin == yMax;

            if (ProcessingMode == ProcessingMode.CPU)
            {
                if (xMin == xMax)
                {
                    for (y = yMin; y <= yMax; y++)
                    {
                        if (CameraInfo.main.IsInScreen(xMin, y))
                        {
                            vertices[(int)(y * width + xMin)].Color = col;
                        }
                    }
                } else if (yMin == yMax)
                {
                    for (x = xMin; x <= xMax; x++)
                    {
                        if (CameraInfo.main.IsInScreen(x, yMin))
                        {
                            vertices[(int)(yMin * width + x)].Color = col;
                        }
                    }
                } else 
                {
                    int deltaX = xMax - xMin;
                    int deltaY = yOfXMax - yOfXMin;
                    if (useX)
                    {
                        for (x = xMin; x <= xMax; x++)
                        {
                            y = deltaY * (x - xMin) / deltaX + yOfXMin;
                            if (CameraInfo.main.IsInScreen(x,y))
                            {
                                vertices[(int)(y * width + x)].Color = col;
                            }
                        }
                    } else
                    {
                        for (y = yMin; y <= yMax; y++)
                        {
                            x = (y - yOfXMin) * deltaX / deltaY + xMin;
                            if (CameraInfo.main.IsInScreen(x, y))
                            {
                                vertices[(int)(y * width + x)].Color = col;
                            }
                        }
                    }
                }
            }
            else
            {
                if (cudaKernelWireframe == null)
                {
                    cudaKernelWireframe = cudaContext.LoadKernel("kernel.ptx", "ComputeSegment");
                }
                int straightLine = yMin == yMax || xMin == xMax ? 1 : 0;
                useX = useX || yMin == yMax;
                int nbThreads = useX ? xMax - xMin + 1 : yMax - yMin + 1;
                cudaKernelWireframe.BlockDimensions = Math.Min(nbThreads, maxThreadsPerBlock);
                cudaKernelWireframe.GridDimensions = (cudaKernelWireframe.BlockDimensions + nbThreads - 1) / cudaKernelWireframe.BlockDimensions;
                int deltaX = xMax - xMin;
                int deltaY = yOfXMax - yOfXMin;
                int useXInt = useX ? 1 : 0;
                cudaKernelWireframe.RunAsync(
                    new ManagedCuda.BasicTypes.CUstream(),
                    d_vertices.DevicePointer, col, width, height, xMin, xMax,
                    yMin, yMax, yOfXMin, yOfXMax, deltaX, deltaY, useXInt, straightLine
                );
            }
        }

        void DrawTriangleWireframe(
            Vector2 screenA, Vector2 screenB, Vector2 screenC,
            Vert a, Vert b, Vert c)
        {
            bool showA = (a.position - CameraInfo.main.position).Dot(CameraInfo.main.front) >= 0;
            bool showB = (b.position - CameraInfo.main.position).Dot(CameraInfo.main.front) >= 0;
            bool showC = (c.position - CameraInfo.main.position).Dot(CameraInfo.main.front) >= 0;
            Color color = GetColorMean(a.color, b.color, c.color);
            if (showA && showB)
                DrawSegment(screenA, screenB, color);
            if (showB && showC)
                DrawSegment(screenB, screenC, color);
            if (showC && showA)
                DrawSegment(screenC, screenA, color);
        }

        public void DrawTriangle(Triangle t) => DrawTriangle(t.a, t.b, t.c);

        public Vector2 ProjectPoint(Vector3 worldPos) => ProjectPoint(worldPos, Matrix4.PerspectiveProjection());

        public Vector2 ProjectPoint(Vector3 worldPos, Matrix4 projectionMatrix)
        {
            Vector3 w;
            return ProjectPoint(worldPos,projectionMatrix,out w);
        }

        public Vector2 ProjectPoint(Vector3 worldPos, Matrix4 projectionMatrix, out Vector3 cameraSpacePos)
        {
            Vector3 screenCenter = CameraInfo.main.center;
            Vector2 offset = new Vector2(1,1);
            Vector3 pPrime = new Vector3();
            Vector3 camPointDist = worldPos - CameraInfo.main.position;
            cameraSpacePos = new Vector3(
                    camPointDist.Dot(CameraInfo.main.right),
                    camPointDist.Dot(CameraInfo.main.up),
                    camPointDist.Dot(CameraInfo.main.front));
            
            if (CameraInfo.main.projectionType == ProjectionType.orthographic)
            {
                pPrime = worldPos - (worldPos - screenCenter).Dot(CameraInfo.main.front) * CameraInfo.main.front;
                Vector3 dist = (pPrime - screenCenter) / 2f;
                pPrime = new Vector3(
                    dist.Dot(CameraInfo.main.right),
                    dist.Dot(CameraInfo.main.up),
                    CameraInfo.main.near
                );
            }
            else
            {
                Vector4 position = new Vector4(cameraSpacePos, 1) * projectionMatrix;
                position /= position.w;
                pPrime = position;
            }

            return (((Vector2)pPrime / CameraInfo.main.size + offset) / 2) * CameraInfo.main.screenSize;
        }

        public void DrawTriangle(Vert a, Vert b, Vert c)
        {
            Vector2 screenA = new Vector2(), screenB = new Vector2(), screenC = new Vector2();
            Vector3 camA, camB, camC;
            Matrix4 projection = Matrix4.PerspectiveProjection();

            screenA = ProjectPoint(a.position, projection, out camA);
            screenB = ProjectPoint(b.position, projection, out camB);
            screenC = ProjectPoint(c.position, projection, out camC);

            int xMin, xMax, yMin, yMax;
            xMin = (int)MathF.Floor(MathF.Min(MathF.Min(screenA.x, screenB.x), screenC.x));
            xMax = (int)MathF.Floor(MathF.Max(MathF.Max(screenA.x, screenB.x), screenC.x));
            yMin = (int)MathF.Floor(MathF.Min(MathF.Min(screenA.y, screenB.y), screenC.y));
            yMax = (int)MathF.Floor(MathF.Max(MathF.Max(screenA.y, screenB.y), screenC.y));

            if (xMin >= width || yMin >= height || xMax < 0 || yMax < 0) return;

            float area = EdgeFunction(screenA, screenB, screenC);
            
            if (CameraInfo.main.renderingMode == RenderingMode.full)
            {
                if (ProcessingMode == ProcessingMode.CPU)
                {
                    DrawTriangleCPU(xMin, xMax, yMin, yMax, area, screenA, screenB, screenC, 
                        new Vert(camA, a.color), new Vert(camB, b.color), new Vert(camC, c.color));
                }
                else
                {
                    int x0 = Math.Max(0, xMin);
                    int xn = Math.Min((int)width-1, xMax);
                    int y0 = Math.Max(0,yMin);
                    int yn = Math.Min((int)height-1, yMax);
                    int L = xn - x0 + 1, H = yn - y0 + 1;

                    if (L <= 0 || H <= 0) return;

                    int nbThreads = L*H;
                    try
                    {
                        int depthTest = useDetphTest ? 1 : 0;
                        if (cudaKernelTriangle == null)
                        {
                            cudaKernelTriangle = cudaContext.LoadKernel("kernel.ptx", "ComputeTriangle");
                        }
                        cudaKernelTriangle.BlockDimensions = Math.Min(nbThreads, maxThreadsPerBlock);
                        cudaKernelTriangle.GridDimensions = (cudaKernelTriangle.BlockDimensions + nbThreads - 1) / cudaKernelTriangle.BlockDimensions;
                        cudaKernelTriangle.RunAsync(
                            new ManagedCuda.BasicTypes.CUstream(),
                            d_vertices.DevicePointer, d_depthMap.DevicePointer, width,
                            new int2(x0,y0), vertices.Length, L, H, area, depthTest,
                            a.color, b.color, c.color, camA.ToFloat3(),
                            camB.ToFloat3(), camC.ToFloat3(),
                            screenA.ToFloat2(), screenB.ToFloat2(), screenC.ToFloat2()
                        );
                    } catch (Exception e)
                    {
                        Console.WriteLine(
                            $"An error occured while calling the GPU rendering function : {e.Message}" + 
                            "\nRenderScreen switched back to CPU mode.");
                        Console.WriteLine(nbThreads);
                        ProcessingMode = ProcessingMode.CPU;
                        DrawTriangleCPU(xMin, xMax, yMin, yMax, area, screenA, screenB, screenC,
                            new Vert(camA, a.color), new Vert(camB, b.color), new Vert(camC, c.color));
                    }
                }
            }
            else if (CameraInfo.main.renderingMode == RenderingMode.wireframe)
            {
                DrawTriangleWireframe(screenA, screenB, screenC, a, b, c);
            }
        }

        public void DrawQuad(Quad quad) => DrawQuad(quad.a, quad.b, quad.c, quad.d);

        public void DrawQuad(Vert a, Vert b, Vert c, Vert d)
        {
            DrawTriangle(a, b, c);
            DrawTriangle(c, d, a);
        }

        public void DrawCoordinateSystem()
        {
            if (coordSystem == CoordSystem.camera || coordSystem == CoordSystem.both)
            {
                Vector2 screenO = 0.01f * CameraInfo.main.screenSize;
                DrawSegment(screenO, screenO + 50 * Vector2.Right, Color.Red);
                DrawSegment(screenO, screenO + 50 * Vector2.Up, Color.Green);
            }
            if (coordSystem == CoordSystem.world || coordSystem == CoordSystem.both)
            {
                float factor = CameraInfo.main.projectionType == ProjectionType.orthographic ? 0.1f : 0.01f;
                Vector3 o = CameraInfo.main.center;
                Vector3 x = o + factor * Vector3.Right;
                Vector3 y = o + factor * Vector3.Up;
                Vector3 z = o + factor * Vector3.Far;
                Matrix4 projection = Matrix4.PerspectiveProjection();
                Vector2 screenO = ProjectPoint(o, projection);
                Vector2 screenX = ProjectPoint(x, projection);
                Vector2 screenY = ProjectPoint(y, projection);
                Vector2 screenZ = ProjectPoint(z, projection);
                DrawSegment(screenO, screenX, Color.Red);
                DrawSegment(screenO, screenY, Color.Green);
                DrawSegment(screenO, screenZ, Color.Blue);
            }
        }
    }
}