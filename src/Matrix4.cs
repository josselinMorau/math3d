using System;

namespace Math3D
{
    ///<summary>
    /// Struct for 4*4 matrix.
    ///</summary>
    public struct Matrix4
    {
        private const int SIZE = 4;

        ///<summary>
        /// The rows of the matrix.
        ///</summary>
        public Array4<Vector4> rows;

        ///<summary>
        /// The transpose of the matrix.
        ///</summary>
        public Matrix4 Transpose {
            get {
                Matrix4 res = this;
                for(int i = 0; i < SIZE; ++i) {
                    for (int j = 0; j < SIZE; ++j) {
                        res[i, j] = this[j, i];
                    }
                }
                return res;
            }
        }

        private Matrix3 GetSubMatrix(int k)
        {
            Matrix3 m3 = new Matrix3();
            int offset;
            for (int i=0; i<3; i++)
            {
                offset = 0;
                for (int j=0; j<3; j++)
                {
                    if (j == k)
                    {
                        offset = 1;
                    }
                    m3[i,j] = this[i+1, j+offset];
                }
            }
            return m3;
        }

        ///<summary>
        /// The determinant of the matrix.
        ///</summary>
        public float Determinant
        {
            get 
            {
                float det = 0;
                float sign = 1;
                for (int i = 0; i<SIZE; i++)
                {
                    det += sign * GetSubMatrix(i).Determinant;
                    sign *= -1;
                }
                return det;
            }
        }

        ///<summary>
        /// The inverse of the matrix.
        ///</summary>
        public Matrix4 Inverse
        {
            get
            {
                float det = Determinant;
                if (det == 0)
                {
                    throw new ArithmeticException("Non-inversible matrix.");
                }

                Matrix4 res = -this;
                res[0,0] = this[1,1];
                res[1,1] = this[0,0];
                return res/det;
            }
        }

        public Matrix4(float[,] values)
        {
            if (values.GetLength(0) != SIZE || values.GetLength(1) != SIZE)
            {
                throw new IndexOutOfRangeException();
            }

            rows = new Array4<Vector4> (
                new Vector4(values[0,0], values[0,1], values[0,2], values[0,3]),
                new Vector4(values[1,0], values[1,1], values[1,2], values[1,3]),
                new Vector4(values[2,0], values[2,1], values[2,2], values[2,3]),
                new Vector4(values[3,0], values[3,1], values[3,2], values[3,3])
            );
        }

        public Matrix4(Vector4 row1, Vector4 row2, Vector4 row3, Vector4 row4)
        {
            rows = new Array4<Vector4>(row1, row2, row3, row4);
        }

        public float this[int i, int j]
        {
            get => rows[i][j];
            set { 
                Vector4 v = rows[i];
                v[j] = value;
                rows[i] = v;
            }
        }

        ///<summary>
        /// Returns a string representation of the matrix.
        ///</summary>
        public override String ToString() 
            => $"[[{this[0, 0]}, {this[0, 1]}, {this[0, 2]}, {this[0, 3]}]\n"
             + $" [{this[1, 0]}, {this[1, 1]}, {this[1, 2]}, {this[1, 3]}]\n"
             + $" [{this[2, 0]}, {this[2, 1]}, {this[2, 2]}, {this[2, 3]}]\n"
             + $" [{this[3, 0]}, {this[3, 1]}, {this[3, 2]}, {this[3, 3]}]]\n";

        public override int GetHashCode() => base.GetHashCode();

        ///<summary>
        /// Checks equality on the given object.
        ///</summary>
        public override bool Equals(object obj)
        {
            if (!(obj is Matrix4))
                return false;

            return (Matrix4)obj == this;
        }

        ///<summary>
        /// Returns the row with the given index.
        ///</summary>
        ///<param name="i">The index of the row.</param>
        public Vector4 GetRow(int i) => rows[i];

        ///<summary>
        /// Returns the column with the given index.
        ///</summary>
        ///<param name="i">The index of the column.</param>
        public Vector4 GetColumn(int i)
        {
            if (i >= SIZE)
            {
                throw new IndexOutOfRangeException();
            }

            return new Vector4(this[0,i], this[1,i], this[2,i], this[3,i]);
        }

        public static Matrix4 PerspectiveProjection()
        {
            float s = 1 / MathF.Tan(CameraInfo.main.fieldOfView / 2);
            float far = CameraInfo.main.far;
            float near = CameraInfo.main.near;
            return new Matrix4(
                new Vector4(s, 0, 0, 0),
                new Vector4(0, s, 0, 0),
                new Vector4(0, 0, -far/(far - near), 1),
                new Vector4(0, 0, -far*near/(far - near), 0)
            );
        }

        ///<summary>
        /// The identity matrix.
        ///</summary>
        public static Matrix4 Identity => new Matrix4(new float[3,3] {{1,0,0}, {0,1,0}, {0,0,1}});

        public static Matrix4 operator +(Matrix4 m1, Matrix4 m2) 
            => new Matrix4(
                m1.GetRow(0) + m2.GetRow(0), 
                m1.GetRow(1) + m2.GetRow(1),
                m1.GetRow(2) + m2.GetRow(2),
                m1.GetRow(3) + m2.GetRow(3));

        public static Matrix4 operator -(Matrix4 m) => new Matrix4(-m.GetRow(0), -m.GetRow(1), -m.GetRow(2), -m.GetRow(3));

        public static Matrix4 operator -(Matrix4 m1, Matrix4 m2) => m1 + (-m2);

        public static Matrix4 operator *(Matrix4 m, float k) => new Matrix4(k*m.GetRow(0), k*m.GetRow(1), k*m.GetRow(2), k*m.GetRow(3));

        public static Matrix4 operator *(float k, Matrix4 m) => m * k;

        public static Matrix4 operator *(Matrix4 m1, Matrix4 m2)
        {
            Matrix4 res = new Matrix4();
            for (int i=0; i<SIZE; i++)
            {
                for (int j=0; j<SIZE; j++)
                {
                    res[i,j] = m1.GetRow(i).Dot(m2.GetColumn(j));
                }
            }
            return res;
        }

        public static Matrix4 operator /(Matrix4 m, float k) => new Matrix4(m.GetRow(0) / k, m.GetRow(1) / k, m.GetRow(2) / k, m.GetRow(3) / k);

        public static bool operator ==(Matrix4 m1, Matrix4 m2)
        {
            for (int i=0; i<SIZE; i++)
            {
                for (int j=0; j<SIZE; j++)
                {
                    if (m1[i,j] != m2[i,j])
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        
        public static bool operator !=(Matrix4 m1, Matrix4 m2) => !(m1 == m2);

        public static implicit operator Matrix4(Matrix2 m) => new Matrix4(m.GetRow(0), m.GetRow(1), new Vector4(0,0,1,0), new Vector4(0,0,0,1));

        public static implicit operator Matrix4(Matrix3 m) => new Matrix4(m.GetRow(0), m.GetRow(1), m.GetRow(2), new Vector4(0,0,0,1));
    }
}