namespace Math3D
{
    public enum ProjectionType
    {
        orthographic,
        perspective
    }
}